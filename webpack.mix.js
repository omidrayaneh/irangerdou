const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css').sourceMaps();
mix.styles([
    'public/admin/dist/css/adminlte.min.css',
    'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700',
    'public/admin/dist/css/bootstrap-rtl.min.css',
    'public/admin/dist/css/custom-style.css',
], 'public/css/all.css')
    .scripts([
        'public/admin/plugins/jquery/jquery.min.js',
        'public/admin/plugins/bootstrap/js/bootstrap.bundle.min.js',
        'public/admin/dist/js/adminlte.js',
        'public/admin/dist/js/demo.js',
    ], 'public/js/all.js').sourceMaps();