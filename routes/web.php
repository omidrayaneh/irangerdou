<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('frontend.home.index');
});*/
Route::get('/', 'Frontend\MainController@index');



Route::get('/home', 'HomeController@index');

Route::group(['middleware' => ['admin']], function(){
    Route::prefix('admin/')->group(function (){
        Route::get('dashboard', 'Backend\AdminController@index')->name('dashboard');
        Route::resource('users', 'Backend\UserController');
        Route::resource('categories', 'Backend\CategoryController');
        Route::resource('slogans', 'Backend\SloganController');
        Route::resource('welcomes', 'Backend\WelcomeController');
        Route::resource('posts', 'Backend\PostController');
        Route::resource('sliders', 'Backend\SliderController');
        Route::resource('abouts', 'Backend\AboutController');
        Route::resource('photos', 'Backend\PhotoController');
        Route::resource('news', 'Backend\NewsController');
        Route::resource('products', 'Backend\ProductController');
        Route::resource('certificates', 'Backend\CertificateController');
        Route::resource('responses', 'Backend\ResponseController');
        Route::resource('galleries', 'Backend\GalleryController');

       // Route::get('requisitions/edited/{id}', 'Backend\RequisitionController@edited')->name('response.edit');
       // Route::delete ('requisitions/delete/{id}', 'Backend\RequisitionController@destroy')->name('response.delete');
        //Route::get('requisitions', 'Backend\RequisitionController@index');

        Route::post('photos/upload', 'Backend\PhotoController@upload')->name('photos.upload');
        Route::post('galleries/upload', 'Backend\GalleryController@upload')->name('galleries.upload');
        Route::post('sliders/slideUpload','Backend\SliderController@slideUpload')->name('sliders.slideUpload');
        Route::post('welcomes/welcomeUpload', 'Backend\WelcomeController@welcomeUpload')->name('welcomes.Upload');
        Route::post('news/upload', 'Backend\NewsController@upload')->name('news.upload');
        Route::post('products/upload', 'Backend\ProductController@upload')->name('products.upload');
        Route::post('certificates/upload', 'Backend\CertificateController@upload')->name('certificates.upload');

    });
});


Route::get('category/{id}', 'Frontend\MainController@getPostByCategory')->name('category.index');
Route::resource('frontend/requisitions', 'Frontend\RequisitionController');
Route::get('frontend/about', 'Frontend\AboutController@index')->name('about');
Route::get('frontend/contact', 'Frontend\ContactController@index')->name('contact');
Route::get('frontend/gallery', 'Frontend\GalleryController@index')->name('gallery');
Route::get('frontend/services', 'Frontend\ServiceController@index')->name('services');
Route::get('frontend/posts/{id}', 'Frontend\MainController@getPostContent')->name('post');
Route::get('frontend/news/{id}', 'Frontend\MainController@getNewsContent')->name('news');
Route::get('frontend/products/{id}', 'Frontend\MainController@getProductContent')->name('product');




Auth::routes();
