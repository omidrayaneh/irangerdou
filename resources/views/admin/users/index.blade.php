@extends('admin.layouts.master')
@section('content')
    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">کاربران</h3>
                        <div class="text-left">
                            <a class="btn btn-app" href="{{route('users.create')}}">
                                <i class="fa fa-plus green"></i> جدید
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-0 ">
                                    <h3>لیست کاربران</h3>
                                    <div>
                                        <div class="box-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">نام</th>
                                                        <th scope="col">ایمیل</th>
                                                        <th scope="col">نقش</th>
                                                        <th scope="col">عملیات</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php $i=1 @endphp
                                                    @foreach($users as $user)
                                                        <tr>
                                                            <th scope="row">{{$i}}</th>
                                                            <td>{{$user->name}}</td>
                                                            <td>{{$user->email}}</td>
                                                            @if($user->is_admin == 1)
                                                                <td>ادمین</td>
                                                            @else
                                                                <td>کاربر عادی</td>
                                                            @endif
                                                            <td class="text-center ">
                                                                <div class="row">
                                                                    <a class="btn-delete blue mt-auto ml-2"
                                                                       onsubmit="return confirm('Do you really want to delete?');"
                                                                       href="{{route('users.edit', $user->id)}}">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    |
                                                                    <form method="post" class="mt-sm"
                                                                          action="/admin/users/{{$user->id}}">
                                                                        @csrf
                                                                        <input type="hidden" name="_method"
                                                                               value="DELETE">
                                                                        <button type="submit" class="btn-delete">
                                                                            <i class="fa fa-trash red"></i>
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @php $i++; @endphp
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                {!! $users->links() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>
@endsection
