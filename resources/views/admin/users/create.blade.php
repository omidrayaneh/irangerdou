@extends('admin.layouts.master')
@section('content')

    <section class="content ">
        <div class="content-wrapper">
            <div class="box-header with-border mt-3 mr-2 ml-2">
                @if($errors->any())
                    <ul>
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li >{{$error}}</li>
                            @endforeach
                        </div>
                    </ul>
                @endif
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">کاربر جدید</h3>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-2 col-md-8 ">
                                    <form id="myForm" method="post" action="/admin/users">
                                        @csrf
                                        <div class="form-group">
                                            <label for="name">نام</label>
                                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                                                   placeholder="نام کاربر ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">ایمیل</label>
                                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                                                   placeholder="ایمیل ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="role"> نقش</label>
                                            <select name="role" class="form-control">
                                                <option value="0">کاربر عادی</option>
                                                <option value="1">مدیر</option>
                                            </select>

                                        </div>
                                        <div class="form-group">
                                            <label for="password">رمز عبور</label>
                                            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror"
                                                   placeholder="رمز عبور ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="password-confirm">تکرار رمز عبور</label>
                                            <input type="password" name="password-confirm" class="form-control @error('password-confirm') is-invalid @enderror"
                                                   placeholder="تکرار رمز عبور ...">
                                        </div>
                                        <button type="submit" onclick="productGallery()"
                                                class="btn btn-success pull-left">ذخیره
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
