@extends('admin.layouts.master')

@section('content')
    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">
                @if($errors->any())
                    <ul>
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li >{{$error}}</li>
                            @endforeach
                        </div>
                    </ul>
                @endif
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">گروه ها</h3>
                        <div class="text-left">
                            <a class="btn btn-app" href="{{route('slogans.create')}}">
                                <i class="fa fa-plus green"></i> جدید
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-2 col-md-6 ">
                                    <form method="post" action="/admin/slogans">
                                        @csrf
                                        <div class="form-group">
                                            <label for="title">نام</label>
                                            <textarea type="text" name="title" class="form-control @error('title') is-invalid @enderror"
                                                      placeholder="شعار سایت را وارد کنید..."></textarea>
                                        </div>
                                        <label>وضعیت انتشار </label>
                                        <br/>
                                        <input type="checkbox" name="status"  data-toggle="toggle" class="form-control ">
                                        <br/>
                                        <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>


@endsection
