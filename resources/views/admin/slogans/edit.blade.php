@extends('admin.layouts.master')

@section('content')
    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">
                @if($errors->any())
                    <ul>
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li >{{$error}}</li>
                            @endforeach
                        </div>
                    </ul>
                @endif
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">شعار ها</h3>

                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-2 col-md-6 ">
                                    <form method="post" action="/admin/slogans/{{$slogan->id}}">
                                        @csrf
                                        <input type="hidden" name="_method" value="PATCH">
                                        <div class="form-group">
                                            <label for="title">نام</label>
                                            <input type="text" name="title"  value="{{$slogan->title}}" class="form-control @error('title') is-invalid @enderror"
                                                   placeholder="عنوان شعار را وارد کنید...">
                                        </div>
                                        <label>وضعیت انتشار
                                            <br>
                                            <input type="checkbox" name="status" @if($slogan->status==1) checked @endif data-toggle="toggle" class="form-control">
                                        </label>

                                        <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>


@endsection
