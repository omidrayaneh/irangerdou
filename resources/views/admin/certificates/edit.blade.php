@extends('admin.layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">
@endsection

@section('content')
    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">
                @if($errors->any())
                    <ul>
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li >{{$error}}</li>
                            @endforeach
                        </div>
                    </ul>
                @endif
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">گواهی نامه ها</h3>

                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-2 col-md-6 ">
                                    <form method="post" action="/admin/certificates/{{$certificate->id}}">
                                        @csrf
                                        <input type="hidden" name="_method" value="PATCH">
                                        <div class="form-group">
                                            <label for="title">نام</label>
                                            <input type="text" name="title"  value="{{$certificate->title}}" class="form-control @error('title') is-invalid @enderror"
                                                   placeholder="عنوان شعار را وارد کنید...">
                                        </div>
                                        <label>وضعیت انتشار
                                            <br>
                                            <input type="checkbox" name="status" @if($certificate->status==1) checked @endif data-toggle="toggle" class="form-control">
                                        </label>

                                        <div class="form-group">
                                            <label for="photo">گالری تصاویر</label>
                                            <input type="hidden" name="photo_id" id="post-photo">
                                            <div id="photo" class="dropzone "></div>
                                        </div>
                                        <div class="form-group">
                                            @if(($certificate->photo_id)!==null)
                                                <div class="col-sm-3" id="updated_photo_{{$certificate->photo->id}}">
                                                    <img class="img-responsive" width="80" src="{{$certificate->photo->path}}">
                                                    <button type="button" class="btn btn-danger" onclick="removeImages({{$certificate->photo->id}})">حذف</button>
                                                </div>
                                            @endif
                                        </div>
                                        <button type="submit" onclick="postGallery()"  class="btn btn-success pull-left">ذخیره</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('/admin/dist/js/dropzone.js')}}"></script>
    <script>
        Dropzone.autoDiscover = false;
        var photosGallery = [];
        var photos = [].concat({{$certificate->photo->pluck('id')}});
        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            url: "{{ route('certificates.upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                photosGallery.push(response.photo_id)
            }
        });
        postGallery = function(){
            document.getElementById('post-photo').value = photosGallery
        };
        removeImages = function(id){
            var index = photos.indexOf(id)
            photos.splice(index, 1);
            document.getElementById('updated_photo_' + id).remove();
        }
    </script>

@endsection

