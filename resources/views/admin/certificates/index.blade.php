@extends('admin.layouts.master')
@section('content')
    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">گواهی نامه</h3>
                        <div class="text-left">
                            <a class="btn btn-app" href="{{route('certificates.create')}}">
                                <i class="fa fa-plus green"></i> جدید
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="alert alert-warning">
                        <div class="text-center ">آخرین  گواهی نامه منتشر شده بر اساس تاریخ انتشار، در صفحه اول سایت قابل مشاهده خواهد بود.</div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-0 ">
                                    <h3>لیست گواهی نامه ها</h3>
                                    <div>
                                        <div class="box-body">
                                            <div class="table-responsive">
                                                <table class="table no-margin">
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th class="text-center">شناسه</th>
                                                        <th>عنوان</th>
                                                        <th class="text-center">ایجاد کننده</th>
                                                        <th class="text-center">وضعیت انتشار</th>
                                                        <th class="text-center">تاریخ انتشار</th>
                                                        <th >عملیات</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php $i=1;  @endphp
                                                    @foreach($certificates as $certificate)
                                                        <tr>
                                                            <td><img src="{{$certificate->photo ? $certificate->photo->path : "http://www.placehold.it/400" }}"  class="img-fluid" width="80"></td>
                                                            <td class="text-center">{{$i}}</td>
                                                            <td >{{$certificate->title}}</td>
                                                            <td class="text-center">{{$certificate->user->name}}</td>
                                                            @if($certificate->status==1)
                                                                <td class="text-center"><span class="label label-success td-admin"> نمایش داده شود</span></td>
                                                            @else
                                                                <td class="text-center"><span class="label label-danger td-admin">نمایش داده نشود</span></td>
                                                            @endif
                                                            <td class="text-center">
                                                                {{\Hekmatinasser\Verta\Verta::instance($certificate->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}
                                                            </td>                                                            <td>
                                                                <div class="row">
                                                                    <a class="btn-delete blue mt-auto ml-2"
                                                                       onsubmit="return confirm('Do you really want to delete?');"
                                                                       href="{{route('certificates.edit', $certificate->id)}}">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    |
                                                                    <form method="post" class="mt-sm"
                                                                          action="/admin/certificates/{{$certificate->id}}">
                                                                        @csrf
                                                                        <input type="hidden" name="_method"
                                                                               value="DELETE">
                                                                        <button type="submit" class="btn-delete">
                                                                            <i class="fa fa-trash red"></i>
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @php $i++; @endphp
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                {!! $certificates->links() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>
@endsection

