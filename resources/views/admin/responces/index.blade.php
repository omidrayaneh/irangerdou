@extends('admin.layouts.master')
@section('content')
    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">درخواست ها</h3>
                        <div class="text-left">

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-0 ">
                                    <div>
                                        <div class="box-body">
                                            <div class="table-responsive">
                                                <table class="table no-margin">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">شناسه</th>
                                                        <th class="text-center">ایجاد کننده</th>
                                                        <th class="text-center"> ایمیل ایجاد کننده</th>
                                                        <th class="text-center">تلفن  ایجاد کننده</th>
                                                        <th class="text-center">وضعیت مشاهده</th>
                                                        <th class="text-center">وضعیت بررسی</th>
                                                        <th class="text-center">تاریخ انتشار</th>
                                                        <th >عملیات</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php $i=1;  @endphp
                                                    @foreach($responces as $responce)
                                                        <tr>
                                                            <td class="text-center">{{$i}}</td>
                                                            <td class="text-center">{{$responce->name}}</td>
                                                            <td class="text-center">{{$responce->email}}</td>
                                                            <td class="text-center">{{$responce->mobile}}</td>
                                                            @if($responce->status==1)
                                                                <td class="text-center"><span class="label label-success td-admin"> مشاهده شده</span></td>
                                                            @else
                                                                <td class="text-center"><span class="label label-danger td-admin">مشاهده نشده</span></td>
                                                            @endif
                                                            @if($responce->response==1)
                                                                <td class="text-center"><span class="label label-success td-admin"> بررسی شده</span></td>
                                                            @else
                                                                <td class="text-center"><span class="label label-danger td-admin">بررسی نشده</span></td>
                                                            @endif
                                                            <td class="text-center">
                                                                {{\Hekmatinasser\Verta\Verta::instance($responce->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}
                                                            </td>                                                            <td>
                                                                <div class="row">
                                                                    <a class="btn-delete blue mt-auto ml-2"
                                                                       onsubmit="return confirm('Do you really want to delete?');"
                                                                       href="{{route('responses.edit', $responce->id)}}">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    |
                                                                    <form method="post" class="mt-sm"
                                                                          action="/admin/responses/{{$responce->id}}">
                                                                        @csrf
                                                                        <input type="hidden" name="_method"
                                                                               value="DELETE">
                                                                        <button type="submit" class="btn-delete">
                                                                            <i class="fa fa-trash red"></i>
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @php $i++; @endphp
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                {!! $responces->links() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>
@endsection

