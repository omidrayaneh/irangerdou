@extends('admin.layouts.master')

@section('content')
    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">
                @if($errors->any())
                    <ul>
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li >{{$error}}</li>
                            @endforeach
                        </div>
                    </ul>
                @endif
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">درخواست {{$response->name}}</h3>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-2 col-md-6 ">
                                    <form method="post" action="/admin/responses/{{$response->id}}">
                                        @csrf
                                        <input type="hidden" name="_method" value="PATCH">
                                        <div class="form-group">
                                            <label for="title">نام</label>
                                            <input disabled type="text" name="name"  value="{{$response->name}}" class="form-control @error('title') is-invalid @enderror"
                                                   placeholder="عنوان درخواست را وارد کنید...">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">ایمیل</label>
                                            <input disabled type="email" name="email"  value="{{$response->email}}" class="form-control @error('title') is-invalid @enderror"
                                                   placeholder="ایمیل درخواست کننده را وارد کنید...">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">تلفن</label>
                                            <input disabled type="text" name="subject"  value="{{$response->mobile}}" class="form-control @error('title') is-invalid @enderror"
                                                   placeholder="تلفن  را وارد کنید...">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">متن درخواست</label>
                                            <input disabled type="text" name="title"  value="{{$response->title}}" class="form-control @error('title') is-invalid @enderror"
                                                   placeholder="متن درخواست را وارد کنید...">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">موضوع درخواست</label>
                                            <input disabled type="text" name="subject"  value="{{$response->subject}}" class="form-control @error('title') is-invalid @enderror"
                                                   placeholder="موضوع درخواست را وارد کنید...">
                                        </div>
                                        <label> وضعیت مشاهده
                                            <br>
                                            <input type="checkbox" name="status" @if($response->status==1) checked @endif data-toggle="toggle" class="form-control">
                                        </label>
                                        <br>
                                        <label>وضعیت بررسی
                                            <br>
                                            <input type="checkbox" name="response" @if($response->response==1) checked @endif data-toggle="toggle" class="form-control">
                                        </label>
                                        <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>


@endsection
