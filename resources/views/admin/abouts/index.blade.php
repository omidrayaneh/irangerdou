@extends('admin.layouts.master')
@section('style')

@endsection
@section('content')

    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">در باره ما</h3>
                        <div class="text-left">
                            <a class="btn btn-app" href="{{route('abouts.create')}}">
                                <i class="fa fa-plus green"></i> جدید
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-0 ">
                                    <h3> درباره ما</h3>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>

@endsection

@section('script')
    <script type="text/javascript" src="{{asset('admin/plugins/ckeditor/ckeditor.js')}}"></script>
@endsection
<script>
    CKEDITOR.replace('Description',{
        customConfig: 'config.js',
        toolbar: 'simple',
        language: 'fa',
       // removePlugins: 'cloudservices, easyimage'
    })
</script>
