@extends('admin.layouts.master')
@section('content')
    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">گروه ها</h3>
                        <div class="text-left">
                            <a class="btn btn-app" href="{{route('categories.create')}}">
                                <i class="fa fa-plus green"></i> جدید
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-0 ">
                                    <h3>لیست گروه ها</h3>
                                    <div>
                                        <div class="box-body">
                                            <div class="table-responsive">
                                                <table class="table no-margin">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">شناسه</th>
                                                        <th>عنوان</th>
                                                        <th>ایجاد کننده</th>
                                                        <th class="text-center">وضعیت انتشار</th>
                                                        <th class="text-center">وضعیت تخصیص پست</th>
                                                        <th class="text-center">تاریخ انتشار</th>
                                                        <th >عملیات</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php $i=1 @endphp
                                                    @foreach($categories as $category)
                                                        <tr>
                                                            <td class="text-center">{{$category->id}}</td>
                                                            <td class="red">{{$category->name}}</td>
                                                            <td >{{$category->user->name}}</td>
                                                            @if($category->status==1)
                                                                <td class="text-center"><span class="label label-success td-admin"> نمایش داده شود</span></td>
                                                            @else
                                                                <td class="text-center"><span class="label label-danger td-admin">نمایش داده نشود</span></td>
                                                            @endif
                                                            @if($category->ended==1)
                                                                <td class="text-center"><span class="label label-success td-admin">پستی ثبت شده</span></td>
                                                            @else
                                                                <td class="text-center"><span class="label label-danger td-admin">پستی ثبت نشده</span></td>
                                                            @endif
                                                            <td class="text-center">
                                                               {{\Hekmatinasser\Verta\Verta::instance($category->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}
                                                            </td>
                                                            <td>
                                                                <div class="row">
                                                                    <a class="btn-delete blue mt-auto ml-2"
                                                                       onsubmit="return confirm('Do you really want to delete?');"
                                                                       href="{{route('categories.edit', $category->id)}}">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    |
                                                                    <form method="post" class="mt-sm"
                                                                          action="/admin/categories/{{$category->id}}">
                                                                        @csrf
                                                                        <input type="hidden" name="_method"
                                                                               value="DELETE">
                                                                        <button type="submit" class="btn-delete">
                                                                            <i class="fa fa-trash red"></i>
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @if(count($category->childrenCategories) > 0)
                                                            @include('admin.partials.category_list', ['categories'=>$category->childrenCategories, 'level'=>1])
                                                        @endif
                                                        @php $i++; @endphp
                                                    @endforeach
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>
@endsection

