@extends('admin.layouts.master')

@section('content')
    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">

                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">ایجاد دسته بندی </h3>
                        <div class="text-left">

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    @if($errors->any())
                        <ul>
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <li >{{$error}}</li>
                                @endforeach
                            </div>
                        </ul>
                    @endif
                    <div class="alert alert-warning">
                        <div class="text-center ">سطح دسته بندی ها در 4 سطح تعریف شده است، لطفا برای جلوگیری ایجاد مشکل تعداد سطوح را رعایت فرمایید</div>
                        <div class="text-center ">دقت فرمایید، مطلب خود را در آخرین زیر دسته مرتبط منتشر نمایید</div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-2 col-md-6 ">
                                    <form method="post" action="/admin/categories">
                                        @csrf
                                        <div class="form-group">
                                            <label for="name">نام</label>
                                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                                                   placeholder="عنوان دسته بندی را وارد کنید...">
                                        </div>
                                        <label>وضعیت انتشار
                                            <br>
                                            <input type="checkbox" name="status"  data-toggle="toggle" class="form-control">
                                        </label>

                                        <div class="form-group">
                                            <label for="category_parent">زیر دسته</label>
                                            <select name="category_parent" id="" class="form-control">
                                                <option value="">دسته اصلی</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @if(count($category->childrenCategories) > 0)
                                                        @include('admin.partials.category', ['categories'=>$category->childrenCategories, 'level'=>1])
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-success pull-left">ذخیره</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>


@endsection
