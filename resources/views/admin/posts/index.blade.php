@extends('admin.layouts.master')
@section('content')
    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">پست ها</h3>
                        <div class="text-left">
                            <a class="btn btn-app" href="{{route('posts.create')}}">
                                <i class="fa fa-plus green"></i> جدید
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <div>{{session('error')}}</div>
                    </div>
                @endif
                <div class="alert alert-warning">
                    <div class="text-center ">آخرین سه مطلب منتشر شده بر اساس تاریخ انتشار، در صفحه اول سایت قابل مشاهده خواهد بود.</div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-0">
                                    <h3>لیست پست ها</h3>
                                    <div>
                                        <div class="box-body">
                                            <div class="table-responsive">
                                                <table class="table no-margin">
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th class="text-center">شناسه</th>
                                                        <th>عنوان</th>
                                                        <th>ایجاد کننده</th>
                                                        <th class="text-center">انتشار در صفحه اول</th>
                                                        <th class="text-center">وضعیت انتشار</th>
                                                        <th class="text-center">تاریخ انتشار</th>
                                                        <th >عملیات</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @php $i=1 @endphp
                                                    @foreach($posts as $post)
                                                        <tr>
                                                            <td><img src="{{$post->photo ? $post->photo->path : "http://www.placehold.it/400" }}" alt="{{$post->title}}" class="img-fluid" width="80"></td>
                                                            <td class="text-center">{{$i}}</td>
                                                            <td>{{$post->title}}</td>
                                                            <td >{{$post->user->name}}</td>
                                                            @if($post->status==1)
                                                                <td class="text-center"><span class="label label-success td-admin"> نمایش داده شود</span></td>
                                                            @else
                                                                <td class="text-center"><span class="label label-danger td-admin">نمایش داده نشود</span></td>
                                                            @endif
                                                            @if($post->first_page==1)
                                                                <td class="text-center"><span class="label label-success td-admin"> نمایش داده شود</span></td>
                                                            @else
                                                                <td class="text-center"><span class="label label-danger td-admin">نمایش داده نشود</span></td>
                                                            @endif
                                                            <td class="text-center">
                                                                {{\Hekmatinasser\Verta\Verta::instance($post->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}
                                                            </td>                                                            <td>
                                                                <div class="row">
                                                                    <a class="btn-delete blue mt-auto ml-2"
                                                                       href="{{route('posts.edit', $post->id)}}">
                                                                        <i class="fa fa-edit"></i>
                                                                    </a>
                                                                    |
                                                                    <form method="post" class="mt-sm"
                                                                          action="/admin/posts/{{$post->id}}">
                                                                        @csrf
                                                                        <input type="hidden" name="_method"
                                                                               value="DELETE">
                                                                        <button type="submit" class="btn-delete">
                                                                            <i class="fa fa-trash red"></i>
                                                                        </button>
                                                                    </form>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @php $i++; @endphp
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                                {!! $posts->links() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>
@endsection

