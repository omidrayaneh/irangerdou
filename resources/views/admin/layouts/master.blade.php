<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>پنل مدیریت</title>
    <link rel="stylesheet" href="/css/all.css">
    <link href="/admin/dist/css/sweetalert.css" type="text/css" rel="stylesheet">
    <link href="/admin/plugins/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/admin/dist/css/sweetalert2.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    @yield('style')
</head>
<body class="hold-transition sidebar-mini">
@include('sweetalert::alert')
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-3">

            <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="جستجو" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </form>

        <!-- Right navbar links -->
        <ul class="navbar-nav mr-auto">
            <!-- Messages Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fa fa-comments-o"></i>
                    <span class="badge badge-danger navbar-badge">@if(count($requisition)!=0){{count($requisition)}}@endif</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-left">
                    <span class="dropdown-item dropdown-header">{{count($requisition)}} درخواست دیده نشده</span>

                @foreach($requisition as $resquis)
                    <a href="{{route('responses.edit',['response'=>$resquis->id])}}" class="dropdown-item">
                        <!-- Message Start -->
                        <div class="media">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                   {{$resquis->name}}
                                </h3>
                                <p class="text-sm">{{$resquis->subject}}</p>
                                <p class="text-sm text-muted"><i class="fa fa-clock-o mr-1"></i>
                                    {{\Hekmatinasser\Verta\Verta::instance($resquis->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}
                                </p>
                            </div>
                        </div>
                        <!-- Message End -->
                    </a>
                    @endforeach
                        <!-- Message End -->
                    <div class="dropdown-divider"></div>
                        @if(count($requisition)==0)
                            <a > درخواستی برای مشاهده وجود ندارد </a>
                        @else
                            <a href="{{route('responses.index')}}" class="dropdown-item dropdown-footer">مشاهده همه درخواست ها</a>
                        @endif

                </div>
            </li>
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell-o"></i>
                    <span class="badge badge-warning navbar-badge">{{count($requisition_response)}}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-left">
                    <span class="dropdown-item dropdown-header">{{count($requisition_response)}} درخواست بررسی نشده</span>
                    <div class="dropdown-divider"></div>
                    @foreach($requisition_response as $res)
                    <a href="{{route('responses.edit',['response'=>$res->id])}}" class="dropdown-item">
                        <i class="fa fa-envelope ml-2"></i>{{$res->name}}
                        <span class="float-left text-muted text-sm">{{$res->subject}}</span>
                    </a>
                    @endforeach
                    @if(count($requisition)==0)
                        <a > درخواستی برای مشاهده وجود ندارد </a>
                    @else
                        <a href="{{route('responses.index')}}" class="dropdown-item dropdown-footer">مشاهده همه درخواست ها</a>
                    @endif                </div>
            </li>
            {{--<li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
                            class="fa fa-th-large"></i></a>
            </li>--}}
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{'/admin/dashboard'}}" class="brand-link">

            <span class="brand-text font-weight-light">پنل مدیریت</span>
        </a>


        <!-- Sidebar -->
        <div class="sidebar">
            <div>
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">

                    <div class="info">
                        <a href="#" class="d-block">سلام! {{Auth::user()->name}}</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                             with font-awesome or any other icon font library -->
                        <li class="nav-item">
                            <a href="{{'/admin/dashboard'}}" class="nav-link">
                                <i class="nav-icon fa fa-dashboard blue"></i>
                                <p>
                                    داشبورد
                                </p>
                            </a>
                        </li>
                        <li class="nav-item has-treeview " >
                            <a href="#" class="nav-link"  >
                                <i class="nav-icon fa fa-gear green"></i>
                                <p>
                                     مدیریت سایت
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('categories.index')}}" class="nav-link">
                                        <i class="fa fa-list-alt "></i>
                                        <p>منوی های اصلی </p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('sliders.index')}}" class="nav-link">
                                        <i class="fa fa-sliders"></i>
                                        <p>اسلاید صفحه اول</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('responses.index')}}" class="nav-link">
                                        <i class="fa fa-list-ul"></i>
                                        <p> درخواست ها <span class="badge badge-danger">{{count($requisition)}}</span> </p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('slogans.index')}}" class="nav-link">
                                        <i class="fa fa-list-ul"></i>
                                        <p>شعار اصلی </p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('welcomes.index')}}" class="nav-link">
                                        <i class="fa fa-list-ul"></i>
                                        <p>شعار خوش آمدگویی </p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('posts.index')}}" class="nav-link">
                                        <i class="fa fa-list-ul"></i>
                                        <p>مطالب </p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('products.index')}}" class="nav-link">
                                        <i class="fa fa-list-ul"></i>
                                        <p>محصولات </p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('news.index')}}" class="nav-link">
                                        <i class="fa fa-image"></i>
                                        <p>اخبار</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="./index.html" class="nav-link">
                                        <i class="fa fa-image"></i>
                                        <p>گالری عکس</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('users.index')}}" class="nav-link">
                                        <i class="fa fa-users"></i>
                                        <p>کاربران</p>
                                    </a>
                                </li>
                            </ul>
                           {{-- <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('contact')}}" class="nav-link">
                                        <i class="fa fa-volume-control-phone"></i>
                                        <p>تماس با ما</p>
                                    </a>
                                </li>
                            </ul>--}}
                          {{--  <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{route('abouts.index')}}" class="nav-link">
                                        <i class="fa fa-connectdevelop"></i>
                                        <p>درباره ما</p>
                                    </a>
                                </li>
                            </ul>--}}
                        </li>
                        <li class="nav-item has-treeview " id="shop">
                            <a href="#" class="nav-link ">
                                <i class="nav-icon fa fa-gear orange" ></i>
                                <p>
                                     مدیریت فروشگاه
                                    <i class="right fa fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview" hidden>
                                <li class="nav-item">
                                    <a href="./index.html" class="nav-link">
                                        <i class="fa fa-list-alt "></i>
                                        <p>گروه بندی</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview" hidden>
                                <li class="nav-item">
                                    <a href="./index.html" class="nav-link">
                                        <i class="fa fa-product-hunt"></i>
                                        <p>کالاها</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview" hidden>
                                <li class="nav-item">
                                    <a href="./index.html" class="nav-link">
                                        <i class="fa fa-list-alt"></i>
                                        <p>فاکتور ها</p>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav nav-treeview" hidden>
                                <li class="nav-item">
                                    <a href="./index.html" class="nav-link">
                                        <i class="fa fa-dollar"></i>
                                        <p>فروش ها</p>
                                    </a>
                                </li>
                            </ul>
                        <li class="nav-item">
                            <a href="{{route('logout')}}" class="nav-link" onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                                <i class="nav-icon fa fa-power-off red"></i>
                                <p>
                                    خروج
                                </p>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </a>
                        </li>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    @yield('content')
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-sm-none d-md-block">
            Anything you want
        </div>
        <!-- Default to the left -->
        <strong>CopyLeft &copy; 2020 <a href="#">امیدرایانه</a>.</strong>
    </footer>
</div>

<script src="{{asset('/js/all.js')}}"></script>
<script src="{{asset('/admin/dist/js/sweetalert.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/admin/dist/js/sweetalert2.all.js')}}" ></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@yield('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        $("#shop").click(function(){
            Swal.fire({
                title: 'خطا!',
                text: 'بعدا مراجعه فرمایید',
                confirmButtonText: 'باشه!',
                icon: 'error',
            })
        });
        $("#success-delete").click(function(){
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'حذف با موفقیت انجام شد',
                showConfirmButton: false,
                timer: 1500
            })
        });
        $("#error-delete").click(function(){
            Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'حذف با موفقیت انجام نشد',
                showConfirmButton: false,
                timer: 1500
            })
        });
    });
</script>
</body>
</html>
