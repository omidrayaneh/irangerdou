@if(isset($selected_category))
    @foreach($categories as $sub_category)
        <option value="{{$sub_category->id}}" @if($selected_category->category_id == $sub_category->id) selected @endif>{{str_repeat(' --- ', $level)}} {{$sub_category->name}}</option>
        @if(count($sub_category->childrenCategories) > 0)
            @include('admin.partials.category', ['categories' => $sub_category->childrenCategories, 'level' => $level+1, 'selected_category'=>$selected_category])
        @endif
    @endforeach
@else
    @foreach($categories as $sub_category)
        <option value="{{$sub_category->id}}" @if($sub_category->ended==1)style="color: red" @endif>{{str_repeat(' --- ', $level)}} {{$sub_category->name}}</option>
        @if(count($sub_category->childrenCategories) > 0)
            @include('admin.partials.category', ['categories' => $sub_category->childrenCategories, 'level' => $level+1])
        @endif
    @endforeach
@endif
