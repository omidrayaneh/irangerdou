@php $i=1 @endphp
@foreach($categories as $sub_category)
    <tr>
        <td class="text-center">{{$sub_category->id}}</td>
        <td >{{str_repeat('---', $level)}} {{$sub_category->name}}</td>
        <td >{{$category->user->name}}</td>
        @if($sub_category->status==1)
            <td class="text-center"><span class="label label-success td-admin">نمایش داده شود</span></td>
        @else
            <td class="text-center"><span class="label label-danger td-admin">نمایش داده نشود</span></td>
        @endif
        @if($sub_category->ended==1)
            <td class="text-center"><span class="label label-success td-admin">پستی ثبت شده</span></td>
        @else
            <td class="text-center"><span class="label label-danger td-admin">پستی ثبت نشده</span></td>
        @endif
        <td class="text-center">
            {{\Hekmatinasser\Verta\Verta::instance($sub_category->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}
        </td>
        <td>
            <div class="row">
                <a class="btn-delete blue mt-auto ml-2"
                   onsubmit="return confirm('Do you really want to delete?');"
                   href="{{route('categories.edit', $sub_category->id)}}">
                    <i class="fa fa-edit"></i>
                </a>
                |
                <form method="post" class="mt-sm"
                      action="/admin/categories/{{$sub_category->id}}">
                    @csrf
                    <input type="hidden" name="_method"
                           value="DELETE">
                    <button type="submit" class="btn-delete">
                        <i class="fa fa-trash red"></i>
                    </button>
                </form>
            </div>
        </td>
    </tr>
    @if(count($sub_category->childrenCategories) > 0)
        @include('admin.partials.category_list', ['categories' => $sub_category->childrenCategories, 'level' => $level+1])
    @endif
    @php $i++; @endphp
@endforeach
