@extends('admin.layouts.master')
@section('content')
    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">پست ها</h3>
                        <div class="text-left">
                            <a class="btn btn-app" href="{{route('sliders.create')}}">
                                <i class="fa fa-plus green"></i> جدید
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">

                            <div class="card ">

                                <div class="card-body offset-md-0 ">
                                    <h3>اسلاید ها</h3>
                                    <div >
                                        <label>ترتیب نمایش اسلاید ها از کوچک به بزرگ</label>
                                    </div>
                                    <div style="" class="slider-column box-body">
                                        @php $i=1 @endphp
                                        @foreach($sliders as $photo)
                                            <div  class="slider-row" id="updated_photo_{{$photo->id}}">
                                                <label>اسلاید شماره {{ $i }}</label>
                                                <img class="img-responsive col-sm-3" style="max-width: 300px;max-height: 200px;" src="/storage/photos/{{$photo->path}}">
                                                <form method="post" action="/admin/sliders/{{$photo->id}}">
                                                    @csrf
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button type="submit" class="btn btn-danger ">حذف</button>
                                                </form>
                                            </div>
                                            @php $i++; @endphp
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        <!-- /.col-md-6 -->
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>
@endsection

