@extends('admin.layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">
@endsection

@section('content')
    <section class="content">
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="box-header with-border mt-3 mr-2 ml-2">
                @if($errors->any())
                    <ul>
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $error)
                                <li >{{$error}}</li>
                            @endforeach
                        </div>
                    </ul>
                @endif
                <div class="col-lg-12">
                    <div class="card ">
                        <h3 class="box-title pull-right mr-2 mt-2">گروه ها</h3>
                        <div class="text-left">
                            <a class="btn btn-app" href="{{route('welcomes.create')}}">
                                <i class="fa fa-plus green"></i> جدید
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card ">
                                <div class="card-body offset-md-2 col-md-6 ">
                                    <form method="post" action="/admin/welcomes">
                                        @csrf
                                        <div class="form-group">
                                            <label for="title">عنوان</label>
                                            <input type="text" name="title" class="form-control @error('title') is-invalid @enderror"
                                                      placeholder="عنوان شعار  را وارد کنید... ">
                                        </div>
                                        <div class="form-group">
                                            <label for="description">توضیحات</label>
                                            <textarea type="text" name="description" class="form-control @error('title') is-invalid @enderror"
                                                      placeholder="توضیحات  شعار  را وارد کنید... "></textarea>
                                        </div>
                                        <label>وضعیت انتشار </label>
                                        <br/>
                                        <input type="checkbox" name="status"  data-toggle="toggle" class="form-control ">
                                        <br/>
                                        <div class="form-group">
                                            <label for="photo">گالری تصاویر</label>
                                            <input type="hidden" name="photo_id" id="welcome-photo">
                                            <div id="photo" class="dropzone "></div>
                                        </div>
                                        <button type="submit" onclick="postGallery()"  class="btn btn-success pull-left">ذخیره</button>
                                    </form>
                                </div>

                            </div>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </section>

@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('/admin/dist/js/dropzone.js')}}"></script>
    <script>
        Dropzone.autoDiscover = false;
        var photosGallery = []
        var drop = new Dropzone('#photo', {
            addRemoveLinks: true,
            url: "{{ route('welcomes.Upload') }}",
            sending: function(file, xhr, formData){
                formData.append("_token","{{csrf_token()}}")
            },
            success: function(file, response){
                photosGallery.push(response.photo_id)
            }
        });
        postGallery = function(){
            document.getElementById('welcome-photo').value = photosGallery
        };


    </script>

@endsection