<div class="product-grids">
    <div class="col-md-6 product-grid wow @if($i%2==1)fadeInRight @else fadeInLeft @endif    ">
        <div class="product-right">
            <img src="{{$product->photo->path}}" class="img-responsive" alt=""/>
        </div>
        <div class="product-left">
            <h4>{{$product->title}}</h4>
            <p>{{$product->detail}}</p>
            <a href=""><i class="glyphicon glyphicon-circle-arrow-left"
                          aria-hidden="true"></i></a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
