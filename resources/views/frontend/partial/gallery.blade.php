<div class="col-md-3 g-right">
<a href="{{$gallery->photo->path}}" rel="title"
   class="b-link-stripe b-animate-go  thickbox">
    <figure class="effect-oscar">
        <img style="width: 280px;height: 280px" src="{{$gallery->photo->path}}" alt=""/>
        <figcaption>
            <h4>{{$gallery->title}}</h4>
            <p>{{$gallery->description}}</p>
        </figcaption>
    </figure>
</a>
</div>
