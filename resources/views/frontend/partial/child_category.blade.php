@if($child_category->status==1)
    <div    style="
    text-decoration: none;
    display: inline-block;
    width: 100%;
        background-color: #17c2a4 ;
        padding: 1em;
    font-family: 'Yekan','Ubuntu Condensed', sans-serif;
        font-size: 1.3em;
    text-align: center;" >
        <a style=" text-decoration: none;" href="{{route('category.index', ['id' => $child_category->id])}}" class="a-child-menu-color">
            {{ $child_category->name }}
            @if (count($child_category->categories)>0)
                <span class="caret"></span>
            @endif
        </a>
    </div>
@endif
@if ($child_category->categories)
    <div class="dropdown-content"  style=" position: relative; " >

        @foreach ($child_category->categories as $childCategory)
            @include('frontend.partial.child_category', ['child_category' => $childCategory])
        @endforeach
    </div>
@endif
