@extends('frontend.layouts.master')

@section('slogan')
    @if($slogan)
        <div class="banner-center">
            <marquee dir="ltr" direction="right" style="font-family: 'Yekan',sans-serif;">
                <h3 style=" font-family: Yekan ,'Ubuntu Condensed', sans-serif;">{{$slogan->title}}</h3>
            </marquee>
        </div>
    @endif
@endsection
@section('banner')
    <div class="banner" >
        <div  >
            @foreach($sliders as $slider)
                <img class="banner-img mySlides "  width="100%"   src="/storage/photos/{{$slider->path}}" alt="ایران گردو">
            @endforeach
        </div>
        @endsection
        @section('content')
            <div class="content">
                <div class="welcome-section">
                    <div class="container">
                        <div class="banner-bottom">
                            <div class="banner-grids">
                                @php $i=1 @endphp
                                @foreach($posts as $post)
                                    <div
                                        class="col-md-4 banner-grid wow  @if($i==1 ) fadeInRight @elseif ($i==2) fadeInDownBig @else fadeInLeft @endif ">
                                        <h4>{{$post->title}}</h4>
                                        <div class="ban1">
                                            <div class="ban-images  view fourth-effect">
                                                <img src="{{$post->photo->path}}" class="img-responsive" alt=""/>
                                                <div class="mask"></div>
                                            </div>
                                            <p>{{$post->short_description}}</p>
                                            <a href="{{route('post',['id'=>$post->id])}}" class="button hvr-wobble-bottom">اطلاعات بیشتر</a>
                                        </div>
                                    </div>
                                    @php $i++ @endphp
                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        @if(count($welcomes)!=0)
                            <h2>خوش آمدید</h2>
                            <div class="welcome-grids">
                                @php $i=1 @endphp
                                @foreach($welcomes as $welcome)
                                    <div
                                        class="col-md-3 welcome-grid wow @if($i==1)fadeInRight @elseif($i==2)fadeInDownBig @elseif($i==3) fadeInUpBig @else fadeInLeft @endif  ">
                                        <div class="welcome-text">
                                            <h4>{{$welcome->title}}</h4>
                                            <p style="min-height: 250px">{{$welcome->description}}</p>
                                        </div>
                                        <div class="welcome-icon">
                                            <img src="{{$welcome->photo->path}}">
                                        </div>
                                    </div>
                                    @php $i++ @endphp
                                @endforeach
                                <div class="clearfix"></div>
                            </div>
                        @endif
                    </div>
                </div>
                <!---welcome-->

                <!---product-->
                @if(count($products)!=0)
                    <div class="product-section">
                        <div class="container">
                            <h3>محصولات محبوب</h3>
                            @php $i=0;$flag=true;@endphp
                            @foreach($products as $product)
                                @if($flag==true)
                                    <div class="product-grids">
                                        @if($i==0)
                                            <div class="col-md-6 product-grid wow fadeInRight ">
                                                <div class="product-right">
                                                    <img src="{{$product->photo->path}}" class="img-responsive" alt=""/>
                                                </div>
                                                <div class="product-left" >
                                                    <h4>{{$product->title}}</h4>
                                                    {!! Str::limit($product->detail,40)  !!}
                                                    <a href="{{route('product',['id'=>$product->id])}}"><i class="glyphicon glyphicon-circle-arrow-left"
                                                                                                           aria-hidden="true"></i></a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            @php $i=1;@endphp
                                        @else
                                            <div class="col-md-6 product-grid wow fadeInLeft ">
                                                <div class="product-right">
                                                    <img src="{{$product->photo->path}}" class="img-responsive" alt=""/>
                                                </div>
                                                <div class="product-left">
                                                    <h4>{{$product->title}}</h4>
                                                    {!! Str::limit($product->detail,40)  !!}
                                                    <a href="{{route('product',['id'=>$product->id])}}"><i class="glyphicon glyphicon-circle-arrow-left"
                                                                                                           aria-hidden="true"></i></a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                            @php $i=0;$flag=false;@endphp
                                        @endif
                                    </div>
                                @else
                                    <div class="product-grids">
                                        @if($i==0)
                                            <div class="col-md-6 product-grid1 wow fadeInRight ">
                                                <div class="product1-right">
                                                    <h4>{{$product->title}}</h4>
                                                    {!! Str::limit($product->detail,40)  !!}
                                                    <a href="{{route('product',['id'=>$product->id])}}"><i class="glyphicon glyphicon-circle-arrow-left"
                                                                              aria-hidden="true"></i></a>
                                                </div>
                                                <div class="product1-left">
                                                    <img src="{{$product->photo->path}}" class="img-responsive" alt=""/>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            @php $i=1;@endphp
                                        @else
                                            <div class="col-md-6 product-grid1 wow fadeInLeft ">
                                                <div class="product1-right">
                                                    <h4>{{$product->title}}</h4>
                                                    {!! Str::limit($product->detail,40)  !!}
                                                    <a href="{{route('product',['id'=>$product->id])}}"><i class="glyphicon glyphicon-circle-arrow-left"
                                                                              aria-hidden="true"></i></a>
                                                </div>
                                                <div class="product1-left">
                                                    <img src="{{$product->photo->path}}" class="img-responsive" alt=""/>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                            @php $i=0;$flag=true; @endphp
                                        @endif
                                    </div>
                                @endif
                            @endforeach


                        </div>
                    </div>
                @endif
            <!---product-->
                @if(count($news)!=0)
                <!---news--->
                    <div class="news-section">
                        <div class="container">
                            <h3>آخرین اخبار</h3>
                            @php $i=1 @endphp
                            @foreach($news as $new)
                                <div class="news-grids wow  @if($i%2==1)fadeInLeft @elseif($i%2==0)fadeInRight @endif  ">
                                    <div class="col-md-4 new-grid">
                                        <div id="box" class="burst-circle teal">
                                            <div class="caption"></div>
                                            <img style="max-height: 250px;" src="{{$new->photo->path }}"
                                                 class="img-responsive"/>
                                            <h4> {{$new->title }} </h4>
                                        </div>
                                    </div>
                                    <div class="col-md-8 new-grid1 hvr-bounce-to-left">
                                        <h5><i class="glyphicon glyphicon-calendar"
                                               aria-hidden="true"></i> {{\Hekmatinasser\Verta\Verta::instance($new->created_at)->formatDifference(\Hekmatinasser\Verta\Verta::today('Asia/Tehran'))}}</h5>
                                        <a href="{{route('news',['id'=>$new->id])}}"> {{$new->short_description }}</a>
                                        {!! Str::limit($new->long_description,50)  !!}                                </div>
                                    <div class="clearfix"></div>
                                </div>
                            @php $i++ @endphp
                        @endforeach

                        <!---news--->
                        </div>
                    </div>
                    <!---news--->
                @endif
            </div>
        @endsection
        @section('script')
            <script>
                var myIndex = 0;
                carousel();

                function carousel() {
                    var i;
                    var x = document.getElementsByClassName("mySlides");
                    if (x==0)
                        return;
                    for (i = 0; i < x.length; i++) {
                        x[i].style.display = "none";
                    }
                    myIndex++;
                    if (myIndex > x.length) {myIndex = 1}
                    x[myIndex-1].style.display = "block";
                    setTimeout(carousel, 5000); // Change image every 2 seconds
                }
            </script>
@endsection
