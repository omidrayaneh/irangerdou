@extends('frontend.layouts.master')

@section('style')
    <script src='https://api.cedarmaps.com/cedarmaps.js/v1.8.1/cedarmaps.js'></script>
    <link href='https://api.cedarmaps.com/cedarmaps.js/v1.8.1/cedarmaps.css' rel='stylesheet' />
@endsection
@section('banner')
    <div class="banner banner1" >
        @include('frontend.partial.banner1')
@endsection
@section('content')
            <div class="content">
                <div class="mail">
                    <div class="container">
                        <h2 >تماس با ما</h2>
                        <div class="mail-grids">
                            <div class="col-md-6 mail-right wow fadeInLeft animated" >
                                <h4>اطلاعات تماس</h4>
                                <p>برای خرید و یا طرح مشکلات باغتان با ما تماس بگیرید. </p>
                                <ul>
                                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>تلفن<span>044-36333282 | 0912-5600577 | 0914-1631490</span></li>
                                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>ایمیل<a href="mailto:info@iran-gerdu.ir">info@iran-gerdu.ir</a></li>
                                </ul>
                                <ul>
                                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>آدرس<span>آذربایجان غربی - شهرستان خوی - خیابان کوچری - روبروی اداره مخابرات</span></li>
                                </ul>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>

                <div class="map wow fadeInDownBig animated" >
                    <h4 style="text-align: center">مکان بر روی نقشه</h4>
                    <br>
                    <div class="container">
                        <div id="map" style="width: 800px; height: 400px;

  display: inline-block; "></div>
                    </div>
@endsection
@section('script')
                        <script>
                            function contactMap() {
                                // Map options
                                var cm_options = {"center":{"lat":38.552646001131514,"lng":44.94415640830994},"maptype":"light","scrollWheelZoom":false,"zoomControl":true,"zoom":17,"minZoom":6,"maxZoom":17,"legendControl":false,"attributionControl":false}
                                // Initialized CedarMap
                                var map = window.L.cedarmaps.map('map', 'https://api.cedarmaps.com/v1/tiles/cedarmaps.streets.json?access_token=ccfb71918b633d807d3124a7b8c15eb1e920a76c', cm_options);
                                // Markers options
                                var markers = [{"popupContent":"ایران گردو","center":{"lat":38.552646001131514,"lng":44.94415640830994},"iconOpts":{"iconUrl":"https://api.cedarmaps.com/v1/markers/marker-square-green.png","iconRetinaUrl":"https://api.cedarmaps.com/v1/markers/marker-square-green@2x.png","iconSize":[82,98]}}];
                                var markersLeaflet = [];
                                var _marker = null;

                                map.setView(cm_options.center, cm_options.zoom);
                                // Add Markers on Map
                                if (markers.length === 0) return;
                                markers.map(function(marker) {
                                    var iconOpts = {
                                        iconUrl: marker.iconOpts.iconUrl,
                                        iconRetinaUrl: marker.iconOpts.iconRetinaUrl,
                                        iconSize: marker.iconOpts.iconSize,
                                        popupAnchor: [0, -49]
                                    };

                                    const markerIcon = {
                                        icon: window.L.icon(iconOpts)
                                    };

                                    _marker = new window.L.marker(marker.center, markerIcon);
                                    markersLeaflet.push(_marker);
                                    if (marker.popupContent) {
                                        _marker.bindPopup(marker.popupContent);
                                    }
                                    _marker.addTo(map);
                                });
                                // Bounding Map to Markers
                                if (markers.length > 1) {
                                    var group = new window.L.featureGroup(markersLeaflet);
                                    map.fitBounds(group.getBounds(), { padding: [30, 30] });
                                }
                            };

                            (function(c,e,d,a){
                                var p = c.createElement(e);
                                p.async = 1; p.src = d;
                                p.onload = a;
                                c.body.appendChild(p);
                            })(document, 'script', 'https://api.cedarmaps.com/cedarmaps.js/v1.8.1/cedarmaps.js', contactMap);
                        </script>
@endsection
