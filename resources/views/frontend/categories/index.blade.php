@extends('frontend.layouts.master')

@section('banner')
    <div class="banner banner1">
        @include('frontend.partial.banner1')
        @endsection
        @section('content')


            <div class="content">
                <div class="about-section">
                    <div class="container">

                        @if(count($cate)!=0)
                            <h4></h4>
                            @foreach($cate as $cat)
                                <div class="about-grids">
                                    @php $i=0; @endphp
                                        <div class="col-md-4 about-grid wow  @if($i==0) fadeInLeft @elseif($i==1) fadeInDownBig @elseif($i==2) fadeInRight @endif  "
                                           >
                                            <h4>
                                                <a href="{{route('category.index', ['id' => $cat->id])}} ">{{$cat->name}}</a>
                                            </h4>
                                        </div>
                                        @php $i++; @endphp
                                        @if($i==3)@php $i=0; @endphp @endif

                                </div>
                            @endforeach
                        @else

                            <div class="about-grids">
                                @php $i=0; @endphp
                                @foreach($posts as $post)
                                    <div class="col-md-4 about-grid wow  @if($i==0) fadeInLeft @elseif($i==1) fadeInDownBig @elseif($i==2) fadeInRight @endif  "
                                        >
                                        <div class="about-img">
                                            <a href="{{route('post',['id'=>$post->id])}}"> <img src="{{$post->photo->path}}" class="img-responsive" alt=""></a>
                                        </div>
                                        <h4>{{$post->title}}</h4>
                                        {!! $post->short_description !!}
                                    </div>
                                    @php $i++; @endphp
                                    @if($i==3)@php $i=0; @endphp @endif
                                @endforeach
                            </div>
                        @endif

                    </div>
                </div>
@endsection
