@extends('frontend.layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('/frontend/css/chocolat.css')}}" type="text/css" media="screen"
          charset="utf-8">
@endsection
@section('banner')
    <div class="banner banner1">
        @include('frontend.partial.banner1')

@endsection
@section('content')

            <div class="content">
                <!--gallery-starts-->
                <div class="gallery wow bounceIn animated" data-wow-delay="0.4s"
                     style="visibility: visible; -webkit-animation-delay: 0.4s;">
                    <div class="container">
                        <div class="gallery-top heading">
                            <h2>گالری</h2>
                        </div>
                        <div class="gallery-bottom grid">
                            @php $i=0;@endphp
                            @foreach($galleries as $gallery)
                                @if($i==0 )
                                    <div class="g-1">
                                @endif
                                        @include('frontend.partial.gallery',['gallery'=>$gallery])
                                 @if($i>=3 )
                                    </div>
                                    <div class="clearfix"></div>
                                @endif
                                    @php $i++ @endphp
                                    @if($i>=4)  @php $i=0; @endphp@endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!--gallery-end-->
    </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('frontend/js/jquery.chocolat.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('.gallery-bottom a').Chocolat();
        });
    </script>
@endsection


