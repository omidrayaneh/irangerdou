<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>ایران گردو</title>
    <!---css--->
    <link href="{{asset('frontend/css/bootstrap.css')}}" rel='stylesheet' type='text/css'/>
    <link href="{{asset('frontend/css/style.css')}}" rel='stylesheet' type='text/css'/>
    <!---css--->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="keywords" content="ایران گردو, نهال گردو, چندلر, گردوی پیوندی,
SmartphChandler ,Fernor,franquette,Howard,Fernette,pedro,احداث باغ,آفت,سفارش نهال پیوندی,نهال پیوندی,سرشاخه کاری,ماشین آلات,iran gerdoo,iran gerdou,
iran gerdo,my gerdo, فرانکت, فرنور, هاوارد, فرنت, پدرو ,گردو,مجیدبرزگر,صفدر افشاری,ارشناسی ارشد باغبانی,استخدام,نهالستان ایران گردو,مرکز تخصصی نهال ایران گردو  ,️معایب پیوند وصله ای گردو,کو پیوند وصله ای,Patch Budding,,️پیوند جوانه وصله ای ,روش انجام پیوند وصله ای,فواید سرشاخه کاری
,ارتباط با نهالستان ایران گردو,دستگاه برداشت گردو و آلبالو,دستگاه پوست گیر گردو,کود دهی درخت گردو,آبیاری درخت گردو,خاک مناسب کاشت نهال گردو,چگونگی پرورش گردو,بهترین زمان کاشت درخت
, احداث باغات مدرت تجاری,سفارش سرشاخه کاری,"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <link href="{{asset('frontend/css/animate.css')}}" rel="stylesheet" type="text/css" media="all">
    <link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
          rel='stylesheet' type='text/css'>
    <link href="/admin/dist/css/sweetalert.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('/admin/dist/css/sweetalert2.css')}}">
    @yield('style')
    <link href="{{asset('frontend/css/w3.css')}}" rel='stylesheet' type='text/css'/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.6/css/mdb.min.css"/>
</head>
<body>
@include('sweetalert::alert')
<!---header--->
<div class="header-section">
    <div class="container-fluid" style="padding-left: 0px;
    padding-right: 0px;">
        <div class="head-bottom">
            <div class="logo  wow fadeInDownBig  ">
                <h1><a href="{{'/'}}" style="font-family: Yekan,serif;">ایران گردو<span>بزرگترین مرکز پرورش نهال گردو در ایران</span></a>
                </h1>
            </div>
        </div>
    </div>
</div>
@yield('banner')
<div class="container-fluid" style=" padding-left: 0px;
    padding-right: 0px;">
    <nav class="navbar navbar-default">
        <div class="container-fluid" style="padding-left: 0px;
    padding-right: 0px;">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-menu" style="background-color: #0b9e85;"
                 id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav ">
                    <li class="nav-item"><a href="{{'/'}}" class="wow fadeInDownBig nav-link" data-wow-delay=".1s">صفحه
                            اصلی<span class="sr-only">(current)</span></a></li>
                    @if($categories)
                        @foreach($categories as $category)
                            <li data-wow-delay=".1s">
                                <a class="wow fadeInDownBig nav-link" data-wow-delay=".1s" href="@if($category->ended==1){{route('category.index', ['id' => $category->id])}}@endif">{{$category->name}}</a>
                                @if($category->childrenCategories)
                                    <ul class="ul-back">
                                        @foreach ($category->childrenCategories as $childCategory)
                                            <li><a href="@if($childCategory->ended==1){{route('category.index', ['id' => $childCategory->id])}}@endif">{{$childCategory->name}}</a>
                                                @if($childCategory->childrenCategories)
                                                    <ul class="ul-back">
                                                        @foreach ($childCategory->childrenCategories as $child_Category)
                                                            <li><a href="@if($child_Category->ended==1){{route('category.index', ['id' => $child_Category->id])}}@endif ">{{$child_Category->name}}</a>
                                                                @if($child_Category->childrenCategories)
                                                                <ul class="ul-back">
                                                                    @foreach ($child_Category->childrenCategories as $end_Category)
                                                                    <li><a href="@if($end_Category->ended==1){{route('category.index', ['id' => $end_Category->id])}}@endif ">{{$end_Category->name}}</a></li>
                                                                    @endforeach
                                                                </ul>
                                                                 @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    @endif
                    <li><a href="{{route('gallery')}}" class="wow fadeInDownBig" data-wow-delay=".1s">گالری عکس</a></li>
                    <li><a href="{{route('contact')}}" class="wow fadeInDownBig" data-wow-delay=".1s">تماس با ما</a>
                    </li>
                    <li><a href="{{route('about')}}" class="wow fadeInDownBig" data-wow-delay=".1s">درباره ما</a></li>
                    <li><a href="{{route('requisitions.create')}}" class="wow fadeInDownBig" data-wow-delay=".1s">ثبت
                            درخواست</a></li>
                    <li><a href="{{route('login')}}" class="wow fadeInDownBig" data-wow-delay=".1s">ورود</a></li>
                </ul>
            </div>
        </div>
    </nav>
    @yield('slogan')
</div>
</div>
@yield('content')
<div class="footer-section">
    <div class="container">
        <div class="footer-grids">
            <div class="col-md-3 footer-grid  fadeInLeft ">
                <h4>درباره ما</h4>
                <ul>
                    <li> تمرکز بر مشتری</li>
                    <li>پرسنل تحصیل کرده</li>
                    <li>تحقیقات علمی و دانشگاهی</li>
                    <li>ثبت اختراع</li>
                    <li>نوآوری</li>

                </ul>
            </div>
            <div class="col-md-3 footer-grid  fadeInDownBig ">
                <h4>راه حل ها</h4>
                <ul>
                    <li>
                        مرکز تماس
                    </li>
                    <li>پشتیبانی مشتریان</li>
                </ul>
            </div>
            <div class="col-md-3 footer-grid  fadeInUpBig ">
                <h4>کارها</h4>
                <ul>
                    <li>
                        پشتیبانی مشتریان
                    </li>
                    <li>
                    <li>آموزش</li>
                    <li>کارگاه های آموزشی</li>
                    <li> آموزش آنلاین</li>
                </ul>
            </div>
            <div class="col-md-3 footer-grid  fadeInLeft ">
                <h4>تماس با ما</h4>
                <p>آذربایجان غربی - خوی</p>
                <p>خیابان کوچری - روبروی اداره مخابرات</p>
                <p>
                    تلفن:04436333282 </p>
                <p>موبایل: 09125600577 | 09141631490</p>
                <a href="mailto:info@irangerdou.ir"> info@irangerdou.ir</a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="copy-section  fadeInLeft ">
    <div class="container">
        <div>
            <a href="https://t.me/Irgerdo"><i class="fa fa-telegram" style="font-size:36px;"></i></a>
            <a href="https://www.instagram.com/irangerdookhoy/"><i class="fa fa-instagram" style="font-size:36px;"></i></a>
        </div>
        <br>
        <p>کلیه حقوق مادی و معنوی برای مجموعه ایران گردو محفوظ می باشد <a href="http://irangerdoo.com/">ایران گردو</a>
        </p>
    </div>
</div>
<script src="{{asset('frontend/js/jquery-1.12.0.min.js')}}"></script>
<script src="{{asset('frontend/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.6/js/mdb.min.js"></script>
<script src="{{asset('frontend/js/wow.min.js')}}"></script>
<script>
    new WOW().init();
</script>
@yield('script')
</body>
</html>
