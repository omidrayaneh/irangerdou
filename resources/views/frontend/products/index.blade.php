@extends('frontend.layouts.master')
@section('banner')
    <div class="banner banner1">
        @include('frontend.partial.banner1')
        @endsection
        @section('content')
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center col-md-12"> <b>{{$product->title}}</b></h1>
                        <div style="padding-right: 50px;
    padding-bottom: 20px;" class="col-md-4">
                            <img width="100%" src="{{$product->photo->path}}" >
                        </div>
                        <div class="col-md-8">
                            <h4><strong>{{$product->detail}}</strong></h4>
                        </div>

                    </div>
                </div>

            </div>
    </div>
    </div>
@endsection



