@extends('frontend.layouts.master')

@section('style')

@endsection
@section('banner')
    <div class="banner banner1">
        @include('frontend.partial.banner1')
        @endsection
        @section('content')
            <div class="content">
                <!--gallery-starts-->
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center col-md-12"> <b>{{$news->title}}</b></h1>
                        <div style="padding-right: 50px;
    padding-bottom: 20px;" class="col-md-4">
                            <img width="100%" src="{{$news->photo->path}}" >
                        </div>
                        <div class="col-md-8">
                            <h4><strong>{{$news->short_description}}</strong></h4>
                        </div>
                        <div class="col-md-8">
                           {!! $news->long_description !!}
                        </div>
                    </div>
                </div>

            </div>
    </div>
    </div>
@endsection
@section('script')
@endsection


