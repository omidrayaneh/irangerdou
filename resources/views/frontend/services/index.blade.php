@extends('frontend.layouts.master')
@section('banner')
    <div class="banner banner1" >
        @include('frontend.partial.banner1')
@endsection
@section('content')
            <div class="content">
                <div class="services">
                    <div class="container">
                        <h2>خدمات</h2>
                        <div class="services-grids" dir=rtl>
                            <div style=" width: 25%;"  class="col-md-3 ser-grid wow fadeInRight " >
                                <h4>
                                    لورم ایپسوم یا طرح‌نما</h4>
                                <ul>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</li>
                                </ul>
                            </div>
                            <div  style=" width: 25%;" class="col-md-3 ser-grid wow fadeInUpBig " >
                                <h4>

                                    لورم ایپسوم یا طرح‌نما</h4>
                                <ul>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                </ul>
                            </div>
                            <div style=" width: 25%;" class="col-md-3 ser-grid wow fadeInDownBig " >
                                <h4>

                                    لورم ایپسوم یا طرح‌نما</h4>
                                <ul>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                </ul>
                            </div>
                            <div style=" width: 25%;" class="col-md-3 ser-grid wow fadeInLeft ">
                                <h4>

                                    لورم ایپسوم یا طرح‌نما</h4>
                                <ul>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                    <li>لورم ایپسوم متن ساختگی با تولید سادگی</a></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="overview">
                    <div class="container">
                        <h3>خدمات کلی</h3>
                        <div class="overview-grids">
                            <div style=" width: 25%;" class="col-md-3 list-grid wow fadeInLeft " >
                                <div class="list-img">
                                    <img src="images/n1.jpg" alt=" " />
                                    <div class="textbox"></div>
                                </div>
                                <h4>

                                    لورم ایپسوم</h4>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                            </div>
                            <div style=" width: 25%;" class="col-md-3 list-grid wow fadeInRight " >
                                <div class="list-img">
                                    <img src="images/n2.jpg" alt=" " />
                                    <div class="textbox"></div>
                                </div>
                                <h4>

                                    لورم ایپسوم</h4>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                            </div>
                            <div  style=" width: 25%;" class="col-md-3 list-grid wow fadeInLeft " >
                                <div class="list-img">
                                    <img src="images/n3.jpg" alt=" " />
                                    <div class="textbox"></div>
                                </div>
                                <h4>

                                    لورم ایپسوم</h4>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                            </div>
                            <div style=" width: 25%;" class="col-md-3 list-grid wow fadeInRight " >
                                <div class="list-img">
                                    <img src="images/n4.jpg" alt=" " />
                                    <div class="textbox"></div>
                                </div>
                                <h4>

                                    لورم ایپسوم</h4>
                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="main-services">
                    <div class="container">
                        <h3>
                            خدمات اصلی</h3>
                        <div  class="services-grids">
                            <div style=" width: 33%;" class="col-md-4 services-grid wow fadeInLeft ">
                                <div class="services-right">
                                    <p>01</p>
                                </div>
                                <div class="services-left">
                                    <h4>

                                        لورم ایپسوم یا طرح‌نما </h4>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div style=" width: 33%;" class="col-md-4 services-grid wow fadeInUpBig " >
                                <div class="services-right">
                                    <p>02</p>
                                </div>
                                <div class="services-left">
                                    <h4>

                                        لورم ایپسوم یا طرح‌نما </h4>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div style=" width: 33%;" class="col-md-4 services-grid wow fadeInLeft " >
                                <div class="services-right">
                                    <p>03</p>
                                </div>
                                <div class="services-left">
                                    <h4>

                                        لورم ایپسوم یا طرح‌نما </h4>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div style=" width: 33%;" class="col-md-4 services-grid wow fadeInRight " >
                                <div class="services-right">
                                    <p>04</p>
                                </div>
                                <div class="services-left">
                                    <h4>

                                        لورم ایپسوم یا طرح‌نما </h4>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div style=" width: 33%;" class="col-md-4 services-grid wow fadeInDownBig " >
                                <div class="services-right">
                                    <p>05</p>
                                </div>

                                <div class="services-left">
                                    <h4>

                                        لورم ایپسوم یا طرح‌نما </h4>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div style=" width: 33%;" class="col-md-4 services-grid wow fadeInLeft " >
                                <div class="services-right">
                                    <p>06</p>
                                </div>
                                <div class="services-left">
                                    <h4>

                                        لورم ایپسوم یا طرح‌نما </h4>
                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
            </div>
    </div>
@endsection




