@extends('frontend.layouts.master')


@section('banner')
    <div class="banner banner1" >
       @include('frontend.partial.banner1')
@endsection
@section('content')
             <div class="content" >
                 <div class="about-section">
                     <div class="container">
                         <h2>درباره ایران گردو</h2>
                         <div class="about-grids">
                             <div class="col-md-4 about-grid wow fadeInLeft animated" data-wow-delay=".5s">
                                 <div class="about-img">
                                     <img src="images/now1.jpg" class="img-responsive" alt=""/>
                                 </div>
                                 <h4>احداث باغ مدرن</h4>
                                 <p>همکاری با شما در جهت احداث باغ مدرن را از ما بخواهید</p>
                             </div>
                             <div class="col-md-4 about-grid wow fadeInDownBig animated" data-wow-delay=".5s">
                                 <div class="about-img">
                                     <img src="images/now2.jpg" class="img-responsive" alt=""/>
                                 </div>
                                 <h4>بررسی مشکلات باغ</h4>
                                 <p>همواره در جهت پربار شدن باغ شما از ابتدا در کنارتان هستیم</p>
                             </div>
                             <div class="col-md-4 about-grid wow fadeInRight animated" data-wow-delay=".5s">
                                 <div class="about-img">
                                     <img src="images/now4.jpg" class="img-responsive" alt=""/>
                                 </div>
                                 <h4>کادر مجرب و تحصیل کرده</h4>
                                 <p>گروه تولیدی ایران گردو بانظارت وزارت کشاورزی و موسسه تحقیقات ثبت وگواهی بذرونهال کشور همواره دنبال ارتقا سطح بهره وری تولیدات خود می باشد</p>
                             </div>
                             <div class="clearfix"></div>
                         </div>
                     </div>
                 </div>

                 <!--//team-->

                 <!--our History-->
                 <div class="history-section">
                     <div class="container">
                         <h3>تاریخچه ما</h3>
                         <div class="history-grids">
                             <div class="col-md-3 history-grid wow fadeInRight animated" data-wow-delay=".5s">
                                 <figure class="icon">
                                     <i class="glyphicon glyphicon-user"></i>
                                 </figure>
                                 <h5>4000</h5>
                                 <h6>مشتریان مبارک</h6>
                             </div>
                             <div class="col-md-3 history-grid wow fadeInUpBig animated" data-wow-delay=".5s">
                                 <figure class="icon">
                                     <i class="glyphicon glyphicon-thumbs-up"></i>
                                 </figure>
                                 <h5>10000</h5>
                                 <h6>بررسی مثبت</h6>
                             </div>
                             <div class="col-md-3 history-grid wow fadeInDownBig animated" data-wow-delay=".5s">
                                 <figure class="icon">
                                     <i class="glyphicon glyphicon-flag"></i>
                                 </figure>
                                 <h5>11</h5>
                                 <h6>
                                     سال تجربه</h6>
                             </div>
                             <div class="col-md-3 history-grid wow fadeInLeft animated" data-wow-delay=".5s">
                                 <figure class="icon">
                                     <i class="glyphicon glyphicon-check"></i>
                                 </figure>
                                 <h5>20</h5>
                                 <h6>کارکنان صمیمی</h6>
                             </div>
                             <div class="clearfix"></div>
                         </div>
                     </div>
                 </div>
                 <!--our History-->

                 <!---Testimonials--->
                 <div class="testimonials-section">
                     <div class="container">
                         <h3>درباره ما</h3>
                         <div class="testimonials-grids">
                             <div class="col-md-2 testimonials-grid1 wow fadeInLeft animated" data-wow-delay=".5s">
                                 <img src="images/us.jpg"   class="img-responsive image-about" alt=""/>
                             </div>
                             <div class="col-md-10 testimonials-grid wow fadeInRight animated" data-wow-delay=".5s">
                                 <p>اولین وتخصصی ترین تولیدکننده نهال گردوی پیوندی ایران بانظارت وزارت جهادکشاورزی وموسسه تحقیقات ثبت وگواهی بذرونهال کشور</p>
                                 <h5>برزگر - افشار</h5>
                             </div>
                             <div class="clearfix"></div>
                         </div>
                     </div>
                 </div>
                 <!---Testimonials--->
             </div>
    </div>
@endsection
