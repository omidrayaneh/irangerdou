@extends('frontend.layouts.master')

@section('banner')
    <div class="banner banner1" >
        @include('frontend.partial.banner1')
@endsection
        @section('content')
            <div class="content">
                <div class="mail">
                    <div class="container">
                        <h2>ثبت درخواست</h2>
                        <div class="mail-grids">
                            <div class="col-md-6 mail-right wow fadeInLeft animated" data-wow-delay=".5s">
                                <h4>اطلاعات تماس</h4>
                                <p>ایران گردو بزرگترین  و تخصصی ترین تولید کننده نهال گردو در ایران</p>
                                <ul>
                                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>تلفن<span>+8 (044) 36 333 282 - 0912 5600577 - 0914 1631490</span></li>
                                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>ایمیل<a href="mailto:info@irangerdoo.com"> info@irangerdoo.com</a></li>
                                </ul>
                                <ul>
                                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>آدرس<span>آذربایجان غربی - شهرستان خوی - خیابان کوچری - روبروی اداره مخابرات</span></li>
                                </ul>
                            </div>
                            <div class="col-md-6 mail-right wow fadeInRight animated" data-wow-delay=".5s">
                                <h4>ارسال پیام</h4>
                                <p>برای ثبت درخواست احداث باغ و یا سفارش نهال و ...، فرم زیر را تکمیل کرده و برای ما ارسال نمایید.</p>
                                <form method="post" action="/frontend/requisitions">
                                    @csrf
                                    <div class="clearfix">
                                        <input type="text" name="name" placeholder="نام و نام خانوادگی" required>
                                        <input type="email" name="email" placeholder="ایمیل" required>
                                        <input type="text" name="mobile" placeholder="تلفن همراه" required>
                                        <input type="text" name="subject" placeholder="موضوع" required>
                                        <textarea name="title" placeholder="نوع متن خود را در اینجا ...." required></textarea>
                                        <input type="submit" value="ارسال">

                                </form>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>

            </div>
@endsection
