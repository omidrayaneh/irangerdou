@extends('frontend.layouts.master')

@section('banner')
    <div class="banner banner1" >
        @include('frontend.partial.banner1')
@endsection

@section('content')

            <div class="content">
                <div class="mail">
                    <div class="container">
                        <h2>تماس با ما</h2>
                        <div class="mail-grids">
                            <div class="col-md-6 mail-right wow fadeInLeft animated" data-wow-delay=".5s">
                                <h4>اطلاعات تماس</h4>
                                <p>ایران گردو بزرگترین  و تخصصی ترین تولید کننده نهال گردو در ایران</p>
                                <ul>
                                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>تلفن<span>+8 (213) 746 820 82</span></li>
                                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>ایمیل<a href="mailto:info@irangerdoo.com">info@irangerdoo.com</a></li>
                                </ul>
                                <ul>
                                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>آدرس<span>تهران-خ شریعتی ابتدای خیابان ملک کوچه ایرانیاد پلاک1</span></li>
                                </ul>
                            </div>
                            <div class="col-md-6 mail-right wow fadeInRight animated" data-wow-delay=".5s">
                                <h4>ارسال پیام</h4>
                                <p>برای ثبت درخواست احداث، فرم زیر را تکمیل کرده و برای ما ارسال نمایید.</p>
                                <form method="post" action="/frontend/requisitions">
                                    @csrf
                                    <input type="text" placeholder="نام و نام خانوادگی" required=" ">
                                    <input type="email" placeholder="ایمیل" required=" ">
                                    <div class="clearfix">
                                        <input type="text" placeholder="شماره همراه" required=" ">
                                        <input  type="text" placeholder="موضوع درخواست" required=" ">
                                    </div>
                                    <textarea placeholder="درخواست خود را در اینجا بنویسید ...." required=" "></textarea>

                                    <button type="submit" >ارسال</button>
                                </form>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>

            </div>
@endsection
