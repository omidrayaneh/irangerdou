<?php

namespace App\Providers;

use App\Category;
use App\Requisition;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*View::composer(
            [ 'dashboard'],
            'requisitions', 'App\Http\View\Composers\Requisitions'
        );*/
        View::composer('admin/*', function ($view) {
            $requisition=Requisition::where('status',0)->get();
            $requisition_response=Requisition::where('response',0)->get();
            $view->with('requisition', $requisition)
                 ->with('requisition_response',$requisition_response);
        });
        View::composer('frontend/*', function ($view) {
            $categories = Category::whereNull('category_id')
                ->with('childrenCategories')
                ->where('status',1)
                ->get();
            $view->with('categories', $categories);
        });
    }
}
