<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $uploads = '/storage/photos/';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getPathAttribute($photo)
    {
        return $this->uploads . $photo;
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function welcomes()
    {
        return $this->hasMany(Welcome::class);
    }

    public function news()
    {
        return $this->hasMany(News::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function certificates()
    {
        return $this->hasMany(Certificate::class);
    }

    public function galleries()
    {
        return $this->hasMany(Gallery::class);
    }
}
