<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public function photo()
    {
        return $this->belongsTo(Photo::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
