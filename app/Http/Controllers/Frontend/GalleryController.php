<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Gallery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index()
    {
        $galleries=Gallery::with('photo')->get();
        return view('frontend.gallery.index',compact('galleries'));
    }
}
