<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Certificate;
use App\Http\Controllers\Controller;
use App\News;
use App\Post;
use App\Product;
use App\Slider;
use App\Slogan;
use App\Welcome;
use App\User;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {

        $welcomes = Welcome::where('status',1)->orderBy('status','DESC')->orderBy('created_at','DESC')->get();
        $certificate =Certificate::with('photo')->
        where('status',1)->
        orderByDesc('created_at')->
        first();
        $products = Product::where('status',1)
            ->orderBy('status','DESC')
            ->orderBy('created_at','DESC')
            ->paginate(10);


        $news=News::with('photo')->
        where('status',1)->
        where('first_page',1)->
        orderByDesc('created_at')->
        limit(10)->
        get();

        $posts=Post::with('photo')->
                     where('status',1)->
                     where('first_page',1)->
                     orderByDesc('created_at')->
                     limit(3)->
                      get();
        $sliders=Slider::all();
        $slogan = Slogan::where('status',1)->
        orderByDesc('created_at')->
        first();
        /*$categories = Category::whereNull('category_id')
            ->with('childrenCategories')
            ->where('status',1)
            ->get();*/
        return view('frontend.home.index',compact(['slogan','posts','sliders','welcomes','news','products','certificate']));
    }
    public function getPostByCategory($id)
    {
        $categories = Category::whereNull('category_id')
            ->with('childrenCategories')
            ->where('status',1)
            ->get();
        $category = Category::whereId($id)->first();
        $cate = Category::where('category_id',$id)->get();
        $posts=Post::with('category','photo')
            ->where('category_id',$category->id)
            ->where('status',1)
            ->get();
        return view('frontend.categories.index', compact([ 'posts','categories','category','cate']));
    }

    public function getPostContent($id)
    {
       $post=Post::with('photo')
            ->where('id',$id)
            ->where('status',1)
            ->first();
        return view('frontend.posts.index', compact([ 'post']));
    }

    public function getNewsContent($id)
    {
        $news=News::with('photo')
            ->where('id',$id)
            ->where('status',1)
            ->first();
        return view('frontend.news.index', compact([ 'news']));
    }

    public function getProductContent($id)
    {
        $product = Product::with('photo')
            ->where('status',1)
            ->first();
        return view('frontend.products.index', compact([ 'product']));
    }

}
