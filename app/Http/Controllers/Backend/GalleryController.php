<?php

namespace App\Http\Controllers\Backend;

use App\Gallery;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateGalleryRequest;
use App\Http\Requests\EditGalleryRequest;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $galleries=Gallery::paginate(10);
        return view('admin.galleries.index',compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.galleries.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateGalleryRequest $request)
    {
        $gallery=new Gallery();
        $gallery->title=$request->input('title');
        $gallery->description=$request->input('description');
        $gallery->photo_id=$request->input('photo_id');
        $gallery->user_id=auth()->user()->id;

        $gallery->save();
        alert()->success('بدون خطا',' پست  '.$gallery->title .' با موفقیت ایجاد شد ');

        return redirect('/admin/galleries');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gallery=with('photo')->Gallery::findOrFail($id);
        return view('admin.galleries.edit',compact('gallery'));
    }

    public function upload(Request $request)
    {

        $uploadedFile = $request->file('file');
        $filename = time().$uploadedFile->getClientOriginalName();
        $original_name = $uploadedFile->getClientOriginalName();

        Storage::disk('local')->putFileAs(
            'public/photos', $uploadedFile, $filename
        );

        $photo = new Photo();
        $photo->original_name = $original_name;
        $photo->path = $filename;
        $photo->user_id = auth()->user()->id;
        $photo->save();

        return response()->json([
            'photo_id' => $photo->id
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditGalleryRequest $request, $id)
    {
        $gallery=Gallery::findOrFail($id);
        $gallery->title=$request->input('title');
        $gallery->description=$request->input('description');
        $gallery->user_id=auth()->user()->id;
        if ($request->input('photo_id')==null){
            if (!isset($gallery->photo->id)){
                alert()->error('خطا',' عکسی را    '.$gallery->title .' انتخاب کنید ');
                return redirect('/admin/galleries/'.$id.'/edit');
            }

        }else{
            $gallery->photo_id=$request->input('photo_id');
        }

        $gallery->save();
        alert()->success('بدون خطا',' عکس  '.$gallery->title .' با موفقیت ویرایش شد ');

        return redirect('/admin/galleries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery=Gallery::findOrFail($id);
        $gallery->delete();
        alert()->success('بدون خطا',' عکس  '.$gallery->title .' با موفقیت حذف شد ');

        return redirect('/admin/galleries');
    }
}
