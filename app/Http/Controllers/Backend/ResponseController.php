<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Requisition;
use Illuminate\Http\Request;

class ResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $responces=Requisition::orderBy('status','ASC')->orderBy('created_at','DESC')->paginate(10);
        return view('admin.responces.index',compact('responces'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       /* $responses=Requisition::orderBy('status','ASC')->orderBy('created_at','DESC')->paginate(10);
        return view('admin.requisitions.index',compact('responses'));*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response=Requisition::findOrFail($id);
        $response->status=1;
        $response->save();
        return view('admin.responces.edit',compact(['response']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requisition=Requisition::findOrFail($id);
        if ($request->input('status')== 'on'){
            $requisition->status=1;
        }
        else{
            $requisition->status=0;
        }
        if ($request->input('response')== 'on'){
            $requisition->response=1;
        }
        else{
            $requisition->response=0;
        }

        $requisition->save();
        alert()->success('بدون خطا',' درخواست   '.$requisition->name .' با موفقیت ویرایش شد ');

        return redirect('/admin/responses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        alert()->error('خطا',' درحال حاضر امکان حذف وجود ندارد ');
        return redirect('/admin/responses');
    }
}
