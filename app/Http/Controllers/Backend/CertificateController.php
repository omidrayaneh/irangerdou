<?php

namespace App\Http\Controllers\Backend;

use App\Certificate;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCertificateRequest;
use App\Http\Requests\EditCertificateRequest;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $certificates = Certificate::with('photo')
            ->orderBy('status','DESC')
            ->orderBy('created_at','DESC')
            ->paginate(10);
        return view('admin.certificates.index', compact(['certificates']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.certificates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCertificateRequest $request)
    {
        $certificate=new Certificate();
        if ($request->input('status')== 'on'){
            $certificate->status=1;
        }
        else{
            $certificate->status=0;
        }
        $certificate->title=$request->input('title');
        $certificate->photo_id=$request->input('photo_id');

        $certificate->user_id=auth()->user()->id;
        $certificate->save();

        alert()->success('بدون خطا','شعار با موفقیت ایجاد شد ');

        return redirect('/admin/certificates');
    }

    public function upload(Request $request)
    {

        $uploadedFile = $request->file('file');
        $filename = time().$uploadedFile->getClientOriginalName();
        $original_name = $uploadedFile->getClientOriginalName();

        Storage::disk('local')->putFileAs(
            'public/photos', $uploadedFile, $filename
        );

        $photo = new Photo();
        $photo->original_name = $original_name;
        $photo->path = $filename;
        $photo->user_id = auth()->user()->id;
        $photo->save();

        return response()->json([
            'photo_id' => $photo->id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $certificate=Certificate::findOrFail($id);
        return view('admin.certificates.edit', compact(['certificate']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCertificateRequest $request, $id)
    {
        $certificate=Certificate::findOrFail($id);
        if (  $request->input('status')=='on' ){
            $certificate->status=1;
        }
        else{
            $certificate->status=0;
        }
        if ($request->input('photo_id')==null){
            if (!isset($certificate->photo->id)){
                alert()->error('خطا',' عکسی را انتخاب کنید ');
                return redirect('/admin/certificates/'.$id.'/edit');
            }
        }else{
            $certificate->photo_id=$request->input('photo_id');
        }
        $certificate->title=$request->input('title');
        $certificate->user_id=auth()->user()->id;
        $certificate->save();
        alert()->success('بدون خطا','شعار  با موفقیت ویرایش شد ');

        return redirect('/admin/certificates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $certificate=Certificate::findOrFail($id);
        $certificate->delete();
        alert()->success('بدون خطا','شعار با موفقیت حذف شد ');
        return redirect('/admin/certificates');
    }
}
