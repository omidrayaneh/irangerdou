<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\EditCategoryRequest;
use App\Http\Requests\EditUserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
Use RealRashid\SweetAlert\Facades\Alert;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::whereNull('category_id')
            ->with('childrenCategories')
            ->get();
        return view('admin.categories.index', compact(['categories']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::whereNull('category_id')
            ->with('childrenCategories')
            ->get();
        return view('admin.categories.create', compact(['categories']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $newCategory=new Category();
       /* $_category='';
        if ($request->input('category_parent')!==null){
            $_category=Category::findOrFail($request->input('category_parent'));
            if ($_category->category_id!==null){
                alert()->error(' خطا','امکان ایجاد زیر گروه برای  دسته '.$_category->name.' وجود ندارد');
                return redirect('/admin/categories');
            }
        }*/
        $newCategory->name = $request->input('name');
        $newCategory->user_id = auth()->user()->id;

        $newCategory->category_id  = $request->input('category_parent');
        if ($request->input('status')== 'on'){
            $newCategory->status=1;
        }
        else{
            $newCategory->status=0;
        }
        $newCategory->save();
        alert()->success('بدون خطا','گروه  '.$newCategory->name.' با موفقیت ایجاد شد ');

        return redirect('/admin/categories');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::with('childrenCategories')
            ->where('category_id', null)
            ->get();
        $category=Category::findOrFail($id);
        return view('admin.categories.edit', compact(['categories','category']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCategoryRequest $request, $id)
    {
        $Category=Category::FindOrFail($id);
        $Category->name = $request->input('name');

        $Category->category_id  = $request->input('category_parent');
        if ($request->input('status')== 'on'){
            $Category->status=1;
        }
        else{
            $Category->status=0;
        }
        $Category->save();
        alert()->success('بدون خطا','گروه  '.$Category->name.' با موفقیت ویرایش شد ');

        return redirect('/admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $category = Category::with('childrenCategories')->where('id', $id)->first();
        if (count($category->childrenCategories) > 0) {
           // toast('برای گروه '.$category->name.' امکان حذف وجود ندارد ','error')->position('bottom-start');
            alert()->error('خطا در حذف','برای گروه '.$category->name.' امکان حذف وجود ندارد ');

            return redirect('/admin/categories');
        }
        $category->delete();
        alert()->success('بدون خطا',' گروه '.$category->name.' با موفقیت حذف شد ');

        return redirect('/admin/categories');
    }
}
