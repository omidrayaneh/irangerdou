<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSloganRequest;
use App\Http\Requests\EditSloganRequest;
use App\Slogan;
use Illuminate\Http\Request;

class SloganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slogans = Slogan::orderBy('status','DESC')->orderBy('created_at','DESC')->paginate(10);
        return view('admin.slogans.index', compact(['slogans']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slogans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSloganRequest $request)
    {


        $newSlogan=new Slogan();
        if ($request->input('status')== 'on'){
            $newSlogan->status=1;
            //Slogan::where('status', 1)->update(['status' => 0]);
        }
        else{
            $newSlogan->status=0;
        }
        $newSlogan->title=$request->input('title');

        $newSlogan->user_id=auth()->user()->id;
        $newSlogan->save();

        alert()->success('بدون خطا','شعار   '.$newSlogan->title.' با موفقیت ایجاد شد ');

       // $slogans=Slogan::all();

        return redirect('/admin/slogans');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slogan=Slogan::findOrFail($id);
        return view('admin.slogans.edit', compact(['slogan']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditSloganRequest $request, $id)
    {

        $slogan=Slogan::findOrFail($id);
        if (  $request->input('status')=='on' ){
           // Slogan::where('status', 1)->update(['status' => 0]);
            $slogan->status=1;
        }
        else{
            $slogan->status=0;
        }
        $slogan->title=$request->input('title');

        $slogan->user_id=auth()->user()->id;
        $slogan->save();

        alert()->success('بدون خطا','شعار   '.$slogan->title.' با موفقیت ویرایش شد ');

        return redirect('/admin/slogans');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slogan=Slogan::findOrFail($id);
        $slogan->delete();
        alert()->success('بدون خطا','شعار   '.$slogan->title.' با موفقیت حذف شد ');
        return redirect('/admin/slogans');
    }
}
