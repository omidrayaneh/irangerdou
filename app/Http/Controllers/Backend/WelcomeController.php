<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateWelcomeRequest;
use App\Http\Requests\EditWelcomeRequest;
use App\Photo;
use App\Welcome;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $welcomes = Welcome::orderBy('status','DESC')->orderBy('created_at','DESC')->paginate();
        return view('admin.welcomes.index', compact(['welcomes']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.welcomes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateWelcomeRequest $request)
    {
        $newwelcome=new Welcome();
        if ($request->input('status')== 'on'){
            $newwelcome->status=1;
        }
        else{
            $newwelcome->status=0;
        }
        $newwelcome->title=$request->input('title');
        $newwelcome->description=$request->input('description');
        $newwelcome->photo_id=$request->input('photo_id');

        $newwelcome->user_id=auth()->user()->id;
        $newwelcome->save();

        alert()->success('بدون خطا','شعار   '.$newwelcome->title.' با موفقیت ایجاد شد ');

        return redirect('/admin/welcomes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function welcomeUpload(Request $request)
    {
        $uploadedFile = $request->file('file');
        $filename = time().$uploadedFile->getClientOriginalName();
        $original_name = $uploadedFile->getClientOriginalName();

        Storage::disk('local')->putFileAs(
            'public/photos', $uploadedFile, $filename
        );

        $photo = new Photo();
        $photo->original_name = $original_name;
        $photo->path = $filename;
        $photo->user_id = auth()->user()->id;
        $photo->save();

        return response()->json([
            'photo_id' => $photo->id
        ]);
    }
    public function edit($id)
    {
        $welcome=Welcome::with('photo')->findOrFail($id);
        return view('admin.welcomes.edit', compact(['welcome']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditWelcomeRequest $request, $id)
    {
        $welcome=Welcome::findOrFail($id);

        if ($request->input('photo_id')==null){
            if (!isset($welcome->photo->id)){
                alert()->error('خطا',' عکسی را برای خوش آمد گویی    '.$welcome->title .' انتخاب کنید ');
                return redirect('/admin/posts/'.$id.'/edit');
            }

        }else{
            $welcome->photo_id=$request->input('photo_id');
        }
        if ($request->input('status')== 'on' ){
            $welcome->status=1;
        }
        else{
            $welcome->status=0;
        }

        $welcome->title=$request->input('title');
        $welcome->description=$request->input('description');

        $welcome->user_id=auth()->user()->id;
        $welcome->save();

        alert()->success('بدون خطا','شعار   '.$welcome->title.' با موفقیت ویرایش شد ');

        return redirect('/admin/welcomes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $welcome=Welcome::findOrFail($id);
        $welcome->delete();
        alert()->success('بدون خطا','شعار   '.$welcome->title.' با موفقیت حذف شد ');
        return redirect('/admin/welcomes');
    }
}
