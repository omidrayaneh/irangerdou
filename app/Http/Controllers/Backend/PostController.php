<?php

namespace App\Http\Controllers\Backend;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\EditPostRequest;
use App\Post;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts=Post::with('photo')->orderBy('status','DESC')->orderBy('created_at','DESC')->paginate(10);
        return view('admin.posts.index',compact(['posts']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::whereNull('category_id')
            ->with('childrenCategories')
            ->where('status',1)
            ->get();
        return view('admin.posts.create',compact(['categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        $category=Category::whereId($request->input('category_id'))->first();

       $post=new Post();
       $post->title=$request->input('title');
       $post->short_description=$request->input('short_description');
       $post->long_description=$request->input('description');
       $post->category_id=$request->input('category_id');
       $post->photo_id=$request->input('photo_id');
       $post->user_id=auth()->user()->id;
        if ($request->input('status')== 'on'){
            $post->status=1;
        }
        else{
            $post->status=0;
        }
        if ($request->input('first_page')== 'on'){
            $post->first_page=1;
        }
        else{
            $post->first_page=0;
        }
        $post->save();
        alert()->success('بدون خطا',' پست  '.$post->title .' با موفقیت ایجاد شد ');

        $category->ended=1;
        $category->save();
        return redirect('/admin/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post=Post::with('photo','category')->findOrFail($id);
        $categories = Category::whereNull('category_id')
            ->with('childrenCategories')
            ->where('status',1)
            ->get();
        return view('admin.posts.edit',compact(['post','categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditPostRequest $request, $id)
    {
        $post=Post::findOrFail($id);
        $post->title=$request->input('title');
        $post->long_description=$request->input('description');
        $post->short_description=$request->input('short_description');
        $post->category_id=$request->input('category_id');
        $post->user_id=auth()->user()->id;
        if ($request->input('photo_id')==null){
           if (!isset($post->photo->id)){
               alert()->error('خطا',' عکسی را برای پست   '.$post->title .' انتخاب کنید ');
               return redirect('/admin/posts/'.$id.'/edit');
           }

        }else{
            $post->photo_id=$request->input('photo_id');
        }
        if ($request->input('status')== 'on'){
            $post->status=1;
        }
        else{
            $post->status=0;
        }
        if ($request->input('first_page')== 'on'){
            $post->first_page=1;
        }
        else{
            $post->first_page=0;
        }

        $post->save();
        alert()->success('بدون خطا',' پست  '.$post->title .' با موفقیت ویرایش شد ');

        return redirect('/admin/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post=Post::findOrFail($id);
       $post->delete();
        alert()->success('بدون خطا',' پست  '.$post->title .' با موفقیت حذف شد ');

        return redirect('/admin/posts');
    }
}
