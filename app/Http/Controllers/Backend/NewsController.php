<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\EditNewsRequest;
use App\News;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news=News::with('photo')->orderBy('status','DESC')->orderBy('created_at','DESC')->paginate(10);
        return view('admin.news.index',compact(['news']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }
    public function upload(Request $request)
    {

        $uploadedFile = $request->file('file');
        $filename = time().$uploadedFile->getClientOriginalName();
        $original_name = $uploadedFile->getClientOriginalName();

        Storage::disk('local')->putFileAs(
            'public/photos', $uploadedFile, $filename
        );

        $photo = new Photo();
        $photo->original_name = $original_name;
        $photo->path = $filename;
        $photo->user_id = auth()->user()->id;
        $photo->save();

        return response()->json([
            'photo_id' => $photo->id
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateNewsRequest $request)
    {
        $new=new News();
        $new->title=$request->input('title');
        $new->short_description=$request->input('short_description');
        $new->long_description=$request->input('description');
        $new->photo_id=$request->input('photo_id');
        $new->user_id=auth()->user()->id;
        if ($request->input('status')== 'on'){
            $new->status=1;
        }
        else{
            $new->status=0;
        }
        if ($request->input('first_page')== 'on'){
            $new->first_page=1;
        }
        else{
            $new->first_page=0;
        }
        $new->save();
        alert()->success('بدون خطا',' خبر با موفقیت ایجاد شد ');

        return redirect('/admin/news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $new=News::with('photo')->findOrFail($id);
        return view('admin.news.edit',compact(['new']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditNewsRequest $request, $id)
    {
        $new=News::findOrFail($id);
        $new->title=$request->input('title');
        $new->long_description=$request->input('description');
        $new->short_description=$request->input('short_description');
        $new->user_id=auth()->user()->id;

        if ($request->input('photo_id')==null){
            if (!isset($new->photo->id)){
                alert()->error('خطا',' عکسی را برای خبر  انتخاب کنید ');
                return redirect('/admin/news/'.$id.'/edit');
            }
        }else{
            $new->photo_id=$request->input('photo_id');
        }
        if ($request->input('status')== 'on'){
            $new->status=1;
        }
        else{
            $new->status=0;
        }
        if ($request->input('first_page')== 'on'){
            $new->first_page=1;
        }
        else{
            $new->first_page=0;
        }

        $new->save();
        alert()->success('بدون خطا',' خبر با موفقیت ویرایش شد ');

        return redirect('/admin/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $new=News::findOrFail($id);
        $new->delete();
        alert()->success('بدون خطا',' پست  با موفقیت حذف شد ');

        return redirect('/admin/news');
    }
}
