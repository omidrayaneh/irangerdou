<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Post;
use App\Requisition;
use App\User;
use Illuminate\Http\Request;
use View;

class AdminController extends Controller
{
    public function index()
    {
        $req=Requisition::all();
        $users=User::all();
        $posts=Post::all();
        return view('admin.dashboard.index',compact(['req','users','posts']));
    }

}
/*View::composer('*', function($view)
{
    $view->with('categories', Requisition::where('status',0)->get());
});*/