<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\EditProductRequest;
use App\Photo;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=Product::with('photo')->orderBy('status','DESC')->orderBy('created_at','DESC')->paginate(10);
        return view('admin.products.index',compact(['products']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        $products=new Product();
        $products->title=$request->input('title');
        $products->detail=$request->input('description');
        $products->photo_id=$request->input('photo_id');
        $products->user_id=auth()->user()->id;
        if ($request->input('status')== 'on'){
            $products->status=1;
        }
        else{
            $products->status=0;
        }
        if ($request->input('first_page')== 'on'){
            $products->first_page=1;
        }
        else{
            $products->first_page=0;
        }
        $products->save();
        alert()->success('بدون خطا',' محصول  '.$products->title .' با موفقیت ایجاد شد ');

        return redirect('/admin/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::with('photo')->findOrFail($id);
        return view('admin.products.edit',compact(['product']));
    }

    public function upload(Request $request)
    {

        $uploadedFile = $request->file('file');
        $filename = time().$uploadedFile->getClientOriginalName();
        $original_name = $uploadedFile->getClientOriginalName();

        Storage::disk('local')->putFileAs(
            'public/photos', $uploadedFile, $filename
        );

        $photo = new Photo();
        $photo->original_name = $original_name;
        $photo->path = $filename;
        $photo->user_id = auth()->user()->id;
        $photo->save();

        return response()->json([
            'photo_id' => $photo->id
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(EditProductRequest $request, $id)
    {
        $product=Product::findOrFail($id);
        $product->title=$request->input('title');
        $product->detail=$request->input('description');
        $product->user_id=auth()->user()->id;
        if ($request->input('photo_id')==null){
            if (!isset($product->photo->id)){
                alert()->error('خطا',' عکسی را برای محصول   '.$product->title .' انتخاب کنید ');
                return redirect('/admin/posts/'.$id.'/edit');
            }

        }else{
            $product->photo_id=$request->input('photo_id');
        }
        if ($request->input('status')== 'on'){
            $product->status=1;
        }
        else{
            $product->status=0;
        }
        if ($request->input('first_page')== 'on'){
            $product->first_page=1;
        }
        else{
            $product->first_page=0;
        }

        $product->save();
        alert()->success('بدون خطا',' محصول  '.$product->title .' با موفقیت ویرایش شد ');

        return redirect('/admin/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::findOrFail($id);
        $product->delete();
        alert()->success('بدون خطا',' محصول  '.$product->title .' با موفقیت حذف شد ');

        return redirect('/admin/products');
    }
}
