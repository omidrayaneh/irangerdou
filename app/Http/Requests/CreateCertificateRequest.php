<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCertificateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|unique:certificates|min:5',
            'photo_id' =>'required',
        ];
    }
    public function messages()
    {
        return [
            'title.required'=>'عنوان گواهی نامه  را وارد کنید',
            'title.unique'=>'عنوان گواهی نامه  قبلا ثبت شده است',
            'title.min'=>'عنوان گواهی نامه  کمتر از 5 کاراکتر می باشد',
            'photo_id.required'=>'عکسی انتخاب کنید',
        ];
    }
}
