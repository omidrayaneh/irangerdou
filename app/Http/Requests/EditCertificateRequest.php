<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditCertificateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|min:5|unique:certificates,title,'.$this->certificate,
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'عنوان گواهی نامه  را وارد کنید',
            'title.unique'=>'عنوان گواهی نامه  تکراری است',
            'title.min'=>'نام گروه کمتر از 5 کاراکتر می باشد',

        ];
    }
}
