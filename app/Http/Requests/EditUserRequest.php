<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>'required|min:3',
           // 'email' => 'email|required|unique:users,id',
            'email' => 'required|email|unique:users,email,'.$this->user,
            //'password' =>'nullable|min:6',
            'password' => 'nullable|min:6|same:password-confirm',
            'password-confirm' =>'nullable|min:6|same:password',
      ];
    }
    public function messages()
    {
        return [
            'name.required'=>'نام کاربری را وارد کنید',
            'name.min'=>'نام کاربری کمتر از سه کاراکتر می باشد',
            'email.required'=>'ایمیل کاربر را وارد کنید',
            'email.unique'=>'ایمیل قبلا ثبت شده است',
            'password.min'=>'رمز عبور کمتر از 6 کاراکتر می باشد',
            'password-confirm.min'=>'تکرار رمز عبور کمتر از 6 کاراکتر می باشد',
            'password.same'=>'تکرار رمز عبور  با رمز عبور برابر نیست',
            'password-confirm.same'=>'تکرار رمز عبور اشتباه است',
        ];
    }
}
