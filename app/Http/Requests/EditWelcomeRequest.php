<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditWelcomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|unique:welcomes,title,'.$this->welcome,
            'description' =>'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'عنوان شعار را وارد کنید',
            'title.unique'=>'عنوان شعار تکراری است',
            'description.required'=>'توضیحات  شعار را وارد کنید',

        ];
    }
}
