<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>'required|unique:categories|min:3',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'نام گروه را وارد کنید',
            'name.unique'=>'نام گروه قبلا ثبت شده است',
            'name.min'=>'نام گروه کمتر از سه کاراکتر می باشد',

        ];
    }
}
