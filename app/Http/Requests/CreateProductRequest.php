<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|min:5|unique:products',
            'description' =>'required|min:20',
            'photo_id' =>'required',

        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'عنوان محصول را وارد کنید',
            'title.unique'=>'عنوان محصول قبلا ثبت شده است',
            'title.min'=>'عنوان محصول کمتر از 5 کارکتر می باشد',
            'description.required'=>'متن توضیحات  محصول را وارد کنید',
            'description.min'=>'متن توضیحات  محصول کمتر از 20 کاراکتر می باشد',
             'photo_id.required'=>'عکسی را برای محصول انتخاب کنید',
        ];
    }
}
