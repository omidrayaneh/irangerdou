<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditGalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|min:5|unique:galleries,title,'.$this->galleri,
            'description' =>'required|max:20',
           // 'photo_id' =>'required',

        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'عنوان عکس را وارد کنید',
            'title.unique'=>'عنوان عکس قبلا ثبت شده است',
            'title.min'=>'عنوان عکس کمتر از 5 کارکتر می باشد',
            'description.required'=>'متن توضیحات عکس را وارد کنید',
            'description.min'=>'متن توضیحات عکس بیشتز از 20 کاراکتر می باشد',
            // 'photo_id.required'=>'عکسی را برای عکس انتخاب کنید',
        ];
    }
}
