<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|min:5|unique:posts',
            'short_description' =>'required|max:20',
            'description' =>'required|min:20',
            'photo_id' =>'required',

        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'عنوان پست را وارد کنید',
            'title.unique'=>'عنوان پست قبلا ثبت شده است',
            'title.min'=>'عنوان پست کمتر از 5 کارکتر می باشد',
            'description.required'=>'متن توضیحات  پست را وارد کنید',
            'description.min'=>'متن توضیحات  پست کمتر از 20 کاراکتر می باشد',
            'short_description.max'=>'متن توضیحات کوتاه پست بیشتر از 20 کاراکتر می باشد',
            'short_description.required'=>'متن توضیحات کوتاه پست را وارد کنید',
             'photo_id.required'=>'عکسی را برای پست انتخاب کنید',
        ];
    }
}
