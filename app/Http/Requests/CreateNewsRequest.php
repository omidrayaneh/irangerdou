<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|min:5|unique:news',
            'short_description' =>'required|max:20',
            'description' =>'required|min:30',
            'photo_id' =>'required',

        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'عنوان خبر را وارد کنید',
            'title.unique'=>'عنوان خبر قبلا ثبت شده است',
            'title.min'=>'عنوان خبر کمتر از 5 کارکتر می باشد',
            'description.required'=>'متن توضیحات  خبر را وارد کنید',
            'description.min'=>'متن توضیحات  خبر کمتر از 30 کاراکتر می باشد',
            'short_description.max'=>'متن توضیحات کوتاه خبر بیشتر از 20 کاراکتر می باشد',
            'short_description.required'=>'متن توضیحات کوتاه خبر را وارد کنید',
             'photo_id.required'=>'عکسی را برای خبر انتخاب کنید',
        ];
    }
}
