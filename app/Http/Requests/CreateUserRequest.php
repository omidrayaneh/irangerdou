<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>'required|min:3',
            'email' =>'required|unique:users',
            'password' =>'required|min:6',
            'password-confirm' =>'required|same:password',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'نام کاربری را وارد کنید',
            'name.min'=>'نام کاربری کمتر از سه کاراکتر می باشد',
            'email.required'=>'ایمیل کاربر را وارد کنید',
            'email.unique'=>'ایمیل قبلا ثبت شده است',
            'password.required'=>'رمز عبور را وارد کنید',
            'password.min'=>'رمز عبور کمتر از 6 کاراکتر می باشد',
            'password-confirm.required'=>'تکرار رمز عبور را وارد کنید',
            'password-confirm.same'=>'تکرار رمز عبور اشتباه است',
        ];
    }
}
