<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditSloganRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|min:5|unique:slogans,title,'.$this->slogan,
        ];
    }

    public function messages()
    {
        return [
            'title.required'=>'عنوان شعار را وارد کنید',
            'title.unique'=>'عنوان شعار تکراری است',
            'title.min'=>'نام گروه کمتر از 5 کاراکتر می باشد',

        ];
    }
}
