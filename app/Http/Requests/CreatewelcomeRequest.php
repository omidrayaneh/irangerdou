<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateWelcomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|unique:welcomes',
            'description' =>'required',
            'photo_id' =>'required',
        ];
    }
    public function messages()
    {
        return [
            'title.required'=>'عنوان شعار را وارد کنید',
            'title.unique'=>'عنوان شعار قبلا ثبت شده است',
            'description.required'=>'توضیحات  شعار را وارد کنید',
            'photo_id.required'=>'عکسی را برای خوش آمدگویی انتخاب کنید',
        ];
    }
}
