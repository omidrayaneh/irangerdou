<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSloganRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|unique:slogans|min:5',
        ];
    }
    public function messages()
    {
        return [
            'title.required'=>'عنوان شعار را وارد کنید',
            'title.unique'=>'عنوان شعار قبلا ثبت شده است',
            'title.min'=>'عنوان شعار کمتر از 5 کاراکتر می باشد',
        ];
    }
}
